package day5

import (
	"bufio"
	"io"
	"log"
	"strings"
	"unicode"
)

func react(polymer string) int {
	simplified := true

	for simplified {
		simplified = false
		for c := range polymer {
			if c == len(polymer)-1 {
				continue
			}
			if (unicode.IsLower(rune(polymer[c])) &&
				rune(polymer[c+1]) == unicode.ToUpper(rune(polymer[c]))) ||
				(unicode.IsUpper(rune(polymer[c])) &&
					rune(polymer[c+1]) == unicode.ToLower(rune(polymer[c]))) {
				// A reaction is due
				polymer = polymer[0:c] + polymer[c+2:]
				simplified = true
				break
			}
		}
	}

	return len(polymer)
}

// Day5Part1 reduces the polymer, and returns the number of characters left
// after the reaction
func Day5Part1(r io.Reader) int {
	scanner := bufio.NewScanner(r)
	var polymer string
	if scanner.Scan() {
		polymer = scanner.Text()
	} else {
		log.Fatal("Failed to read the polymer")
		return -1
	}
	return react(polymer)
}

// Day5Part2 returns the shortest length of polymer, after removing possible faults
func Day5Part2(r io.Reader) int {
	scanner := bufio.NewScanner(r)
	var polymer string
	if scanner.Scan() {
		polymer = scanner.Text()
	} else {
		log.Fatal("Failed to read the polymer")
		return -1
	}
	shortest := len(polymer)
	for c := 'a'; c <= 'z'; c++ {
		newPolymer := strings.Replace(polymer, string(c), "", -1)
		newPolymer = strings.Replace(newPolymer, string(unicode.ToUpper(c)), "", -1)
		l := react(newPolymer)
		if l < shortest {
			shortest = l
		}
	}
	return shortest
}
