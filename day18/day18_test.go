package day18

import (
	"strings"
	"testing"
)

func TestPart1(t *testing.T) {
	input := strings.NewReader(`.#.#...|#.
.....#|##|
.|..|...#.
..|#.....#
#.#|||#|#|
...#.||...
.|....|...
||...#|.#|
|.||||..|.
...#.|..|.`)
	expected := 1147

	SetDebug(true)
	answer := Part1(input)
	if answer != expected {
		t.Errorf("Expected %d, got %d instead", expected, answer)
	}
}
