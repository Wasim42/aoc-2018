package day18

import (
	"bufio"
	"fmt"
	"io"
	"runtime"
	"sync"
)

func parseArea(r io.Reader) *[][]rune {
	area := [][]rune{}
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		if len(scanner.Text()) < 1 {
			continue
		}
		row := make([]rune, len(scanner.Text()))
		for i, c := range scanner.Text() {
			row[i] = c
		}
		area = append(area, row)
	}
	return &area
}

func printArea(area *[][]rune) {
	for _, row := range *area {
		for _, c := range row {
			fmt.Printf("%c", c)
		}
		fmt.Println()
	}
}

func applyRules(x, y int, area, newArea *[][]rune, wg *sync.WaitGroup) {
	defer wg.Done()
	var minx, miny, maxx, maxy int
	maxy = len(*newArea)
	maxx = len((*newArea)[0])
	// Counts of the surrounding
	open := 0
	trees := 0
	lumber := 0
	// 0 1 2
	// 3 X 4
	// 5 6 7
	sx := []int{-1, 0, 1, -1, 1, -1, 0, 1}
	sy := []int{-1, -1, -1, 0, 0, 1, 1, 1}
	for i := range sx {
		dx := x + sx[i]
		dy := y + sy[i]
		if dx >= minx && dx < maxx && dy >= miny && dy < maxy {
			switch (*area)[dy][dx] {
			case '.':
				open++
			case '|':
				trees++
			case '#':
				lumber++
			}
		}
	}
	victim := (*area)[y][x]
	outcome := victim
	switch victim {
	case '.':
		if trees >= 3 {
			outcome = '|'
		}
	case '|':
		if lumber >= 3 {
			outcome = '#'
		}
	case '#':
		if lumber > 0 && trees > 0 {
			outcome = '#'
		} else {
			outcome = '.'
		}
	}
	(*newArea)[y][x] = outcome
}

func doRun(area *[][]rune) *[][]rune {
	var wg sync.WaitGroup
	newArea := [][]rune{}
	for _, r := range *area {
		newArea = append(newArea, make([]rune, len(r)))
	}

	// Now apply the rules to each square in area
	for y := range *area {
		for x := range (*area)[y] {
			wg.Add(1)
			go applyRules(x, y, area, &newArea, &wg)
		}
	}
	wg.Wait()
	return &newArea
}

var debug bool

// SetDebug enables (or disables) debug
func SetDebug(d bool) {
	debug = d
}

func resValue(area *[][]rune) int {
	trees := 0
	lumber := 0
	for _, row := range *area {
		for _, c := range row {
			switch c {
			case '|':
				trees++
			case '#':
				lumber++
			}
		}
	}
	return trees * lumber
}

// Part1 returns the resource value after 10 minutes
func Part1(r io.Reader) int {
	runtime.GOMAXPROCS(runtime.NumCPU())
	area := parseArea(r)
	if debug {
		printArea(area)
	}
	for i := 0; i < 10; i++ {
		area = doRun(area)
		if debug {
			fmt.Println("After", i+1, "minutes:")
			printArea(area)
		}
	}
	return resValue(area)
}

// Part2 returns the resource value after 1000000000 minutes!
func Part2(r io.Reader) int {
	// Note that after a while, the pattern repeats - so we just need to find
	// the frequency of repetition, and then use that to work out which number
	// will be the right one
	iterations := 1000000000
	area := parseArea(r)
	if debug {
		printArea(area)
	}
	steady := make([]int, 1)
	i := 1
	doneSteady := false
	for ; ; i++ {
		area = doRun(area)
		// We assume a steady state by 1000 iterations
		if i == 1000 {
			steady[0] = resValue(area)
		}
		if i > 1000 && !doneSteady {
			examine := resValue(area)
			if examine == steady[0] {
				doneSteady = true
				break
			} else {
				steady = append(steady, examine)
			}
		}
	}
	return steady[(iterations-1000)%len(steady)]
}
