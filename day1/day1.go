package day1

import (
	"bufio"
	"fmt"
	"io"
)

func Day1Part1(r io.Reader) (total int) {
	scanner := bufio.NewScanner(r)
	var num int
	for scanner.Scan() {
		fmt.Sscanf(scanner.Text(), "%d", &num)
		total += num
	}
	return
}

func Day1Part2(r io.Reader) (total int) {
	var num int
	reached := make(map[int]int, 0)
	numbers := make([]int, 0)

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		fmt.Sscanf(scanner.Text(), "%d", &num)
		numbers = append(numbers, num)
	}
	for {
		for _, num := range numbers {
			total += num
			_, ok := reached[total]
			if ok {
				// This is the second time we've seen it
				return
			} else {
				reached[total] = 0
			}

		}
	}

}
