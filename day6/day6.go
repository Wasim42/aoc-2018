package day6

import (
	"bufio"
	"fmt"
	"io"
)

// Abs returns the absolute value of x
func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func parseCoords(r io.Reader) ([]complex64, complex64) {
	scanner := bufio.NewScanner(r)
	coords := make([]complex64, 0)
	var biggestR, biggestI int
	for scanner.Scan() {
		var r, i int
		fmt.Sscanf(scanner.Text(), "%d, %d", &r, &i)
		coords = append(coords, complex(float32(r), float32(i)))
		if r > biggestR {
			biggestR = r
		}
		if i > biggestI {
			biggestI = i
		}
	}
	return coords, complex(float32(biggestR), float32(biggestI))
}

// ManDistance returns the Manhattan Distance between two points
func ManDistance(a, b complex64) int {
	dist := a - b
	return Abs(int(real(dist))) + Abs(int(imag(dist)))
}

// Day6Part2 returns the size of the area bounded by the limit
func Day6Part2(r io.Reader, limit int) int {
	coords, boundary := parseCoords(r)

	bigX := int(real(boundary))
	bigY := int(imag(boundary))
	totalArea := 0
	for x := 0; x < bigX; x++ {
		for y := 0; y < bigY; y++ {
			loc := complex(float32(x), float32(y))
			dist := 0
			for _, c := range coords {
				dist += ManDistance(loc, c)
			}
			if dist < limit {
				totalArea++
			}
		}
	}

	return totalArea

}

// Day6Part1 returns the largest area
func Day6Part1(r io.Reader) int {
	coords, boundary := parseCoords(r)

	bigX := int(real(boundary))
	bigY := int(imag(boundary))
	playBoard := make([][]complex64, bigX)
	for i := range playBoard {
		playBoard[i] = make([]complex64, bigY)
	}

	for x := range playBoard {
		for y := range playBoard[x] {
			shortest := bigX + bigY
			loc := complex(float32(x), float32(y))

			for c := range coords {
				dist := ManDistance(loc, coords[c])
				if dist < shortest {
					shortest = dist
					playBoard[x][y] = coords[c]
				} else if dist == shortest {
					// Two locations at the same distance, reset it!
					playBoard[x][y] = 0 + 0i
				}
			}
		}
	}

	// Remove the unbounded entries - they are the ones along the border
	unbounded := make(map[complex64]int, 0)
	for x := range playBoard {
		unbounded[playBoard[x][0]] = 1
		unbounded[playBoard[x][bigY-1]] = 1
	}
	for y := range playBoard[0] {
		unbounded[playBoard[0][y]] = 1
		unbounded[playBoard[bigX-1][y]] = 1
	}

	bounded := make(map[complex64]int, 0)
	for x := range playBoard {
		for y := range playBoard[x] {
			if _, ok := unbounded[playBoard[x][y]]; !ok {
				// Not in the unbounded list, so worth considering
				bounded[playBoard[x][y]] = bounded[playBoard[x][y]] + 1
			}
		}
	}

	biggest := 0
	for _, size := range bounded {
		if size > biggest {
			biggest = size
		}
	}
	return biggest
}
