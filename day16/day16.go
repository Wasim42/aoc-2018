package day16

import (
	"bufio"
	"fmt"
	"io"
	"reflect"
)

type register []int
type operation []int

const (
	a = 1
	b = 2
	c = 3
)

func parseInput(r io.Reader) ([]register, []operation, []register, []operation) {
	scanner := bufio.NewScanner(r)

	befores := make([]register, 0)
	ops := make([]operation, 0)
	afters := make([]register, 0)
	program := make([]operation, 0)
	for scanner.Scan() {
		if len(scanner.Text()) == 0 {
			continue
		}
		if scanner.Text()[0] == 'B' {
			// This is one of our known before and after blocks
			before := make(register, 4)
			fmt.Sscanf(scanner.Text(), "Before: [%d, %d, %d, %d]",
				&before[0], &before[1], &before[2], &before[3])
			befores = append(befores, before)
			scanner.Scan()
			op := make(operation, 4)
			fmt.Sscanf(scanner.Text(), "%d %d %d %d",
				&op[0], &op[1], &op[2], &op[3])
			ops = append(ops, op)
			scanner.Scan()
			if scanner.Text()[0:5] != "After" {
				panic("Invalid Before/After block")
			}
			after := make(register, 4)
			fmt.Sscanf(scanner.Text(), "After: [%d, %d, %d, %d]",
				&after[0], &after[1], &after[2], &after[3])
			afters = append(afters, after)

		}

		// It's time to parse the program!
		prog := make(operation, 4)
		fmt.Sscanf(scanner.Text(), "%d %d %d %d",
			&prog[0], &prog[1], &prog[2], &prog[3])
		program = append(program, prog)
	}
	return befores, ops, afters, program
}

func addr(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	after[op[c]] = before[op[a]] + before[op[b]]
	return after
}

func addi(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	after[op[c]] = before[op[a]] + op[b]
	return after
}

func mulr(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	after[op[c]] = before[op[a]] * before[op[b]]
	return after
}

func muli(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	after[op[c]] = before[op[a]] * op[b]
	return after
}

func banr(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	after[op[c]] = before[op[a]] & before[op[b]]
	return after
}

func bani(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	after[op[c]] = before[op[a]] & op[b]
	return after
}

func borr(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	after[op[c]] = before[op[a]] | before[op[b]]
	return after
}

func bori(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	after[op[c]] = before[op[a]] | op[b]
	return after
}

func setr(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	after[op[c]] = before[op[a]]
	return after
}

func seti(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	after[op[c]] = op[a]
	return after
}

func gtir(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	if op[a] > before[op[b]] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	return after
}

func gtri(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	if before[op[a]] > op[b] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	return after
}

func gtrr(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	if before[op[a]] > before[op[b]] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	return after
}

func eqir(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	if op[a] == before[op[b]] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	return after
}

func eqri(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	if before[op[a]] == op[b] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	return after
}

func eqrr(before register, op operation) register {
	after := make(register, len(before))
	copy(after, before)
	if before[op[a]] == before[op[b]] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	return after
}

// Part1 returns the number of "like 3 or more opcodes"
func Part1(r io.Reader) int {
	befores, ops, afters, _ := parseInput(r)
	opcodes := []func(register, operation) register{
		addr, addi,
		mulr, muli,
		banr, bani,
		borr, bori,
		setr, seti,
		gtir, gtri, gtrr,
		eqir, eqri, eqrr,
	}

	totals := 0
	for b := range befores {
		count := 0
		for _, f := range opcodes {
			answer := f(befores[b], ops[b])
			if reflect.DeepEqual(answer, afters[b]) {
				count++
			}
		}
		if count >= 3 {
			totals++
		}
	}
	return totals
}

// Part2 returns the value of register 0 after running
func Part2(r io.Reader) int {
	befores, ops, afters, program := parseInput(r)
	opcodeCandidates := []map[string]func(register, operation) register{}

	for i := 0; i < 16; i++ {
		opcodes := map[string]func(register, operation) register{
			"addr": addr, "addi": addi,
			"mulr": mulr, "muli": muli,
			"banr": banr, "bani": bani,
			"borr": borr, "bori": bori,
			"setr": setr, "seti": seti,
			"gtir": gtir, "gtri": gtri, "gtrr": gtrr,
			"eqir": eqir, "eqri": eqri, "eqrr": eqrr,
		}
		opcodeCandidates = append(opcodeCandidates, opcodes)
	}
	for b := range befores {
		opcodes := opcodeCandidates[ops[b][0]]
		for i, f := range opcodes {
			answer := f(befores[b], ops[b])
			if !reflect.DeepEqual(answer, afters[b]) {
				delete(opcodes, i)
			}
		}
		opcodeCandidates[ops[b][0]] = opcodes
	}

	// Unfortunately we don't get totally unique entries, so we now have to
	// eliminate them one, by, one

	// We do this by continuously looping through the opcode candidates,
	// finding any opcode numbers that have only one candidate, and taking it
	// away from all the others

	// We will store our opcode translation table here for later.
	opcodes := make([]func(register, operation) register, 16)
	for {
		finished := true

		var delfunc func(register, operation) register
		var delidx string
		for i := range opcodeCandidates {
			if len(opcodeCandidates[i]) == 1 {
				finished = false
				// fmt.Print("Opcode ", i, " is ")
				for o := range opcodeCandidates[i] {
					delidx = o
					delfunc = opcodeCandidates[i][delidx]
				}
				// Handle this one
				opcodes[i] = delfunc
				break
			}

		}
		if delfunc != nil {
			for i := range opcodeCandidates {
				delete(opcodeCandidates[i], delidx)
			}
		}
		if finished {
			break
		}
	}

	// run

	// I'm assuming we start with an all-zero register
	reg := register{0, 0, 0, 0}

	for _, op := range program {
		f := opcodes[op[0]]
		reg = f(reg, op)
	}

	// fmt.Println("Register:", reg)

	return reg[0]
}
