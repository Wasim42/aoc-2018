package day16

import (
	"strings"
	"testing"
)

func TestDay16Part1(t *testing.T) {
	input := strings.NewReader(`
Before: [3, 2, 1, 1]
9 2 1 2
After:  [3, 2, 2, 1]`)
	expected := 1

	answer := Part1(input)
	if answer != expected {
		t.Errorf("Expected %d, got %d instead", expected, answer)
	}
}
