package day3

import (
	"bufio"
	"fmt"
	"io"
	"log"
)

var fabric1 [1000][1000]int

func makeClaim(posx, posy, width, length int) (overlap int) {
	// Just go through the rectangle covered by the values passed in on fabric1,
	// setting each value

	for x := posx; x < posx+width; x++ {
		for y := posy; y < posy+length; y++ {
			if fabric1[x][y] == 1 {
				overlap++
			}
			fabric1[x][y]++
		}
	}
	return
}

type sclaim struct {
	id     int
	x      int
	y      int
	width  int
	length int
}

// Day3Part1 return the overlapping area given a reader to a file
func Day3Part1(r io.Reader) int {
	claims, err := readClaims(r)
	if err != nil {
		log.Fatal(err)
		return -1
	}
	total := 0
	for _, c := range claims {
		total += makeClaim(c.x, c.y, c.width, c.length)
	}

	return total
}

var fabric2 [1000][1000]int

func stakeClaim(c sclaim) {
	// Just go through the rectangle covered by the values passed in on fabric2,
	// claiming that square for the id

	for x := c.x; x < c.x+c.width; x++ {
		for y := c.y; y < c.y+c.length; y++ {
			fabric2[x][y] = c.id
		}
	}
}

func verifyClaim(c sclaim) bool {
	// Just go through the rectangle covered by the values passed in on fabric2,
	// ensuring that only this owns it.  Also overwrite the values underneath
	// so that the last one to write doesn't get dibs
	success := true
	for x := c.x; x < c.x+c.width; x++ {
		for y := c.y; y < c.y+c.length; y++ {
			if fabric2[x][y] != c.id {
				success = false
			}
			fabric2[x][y] = c.id
		}
	}
	return success
}

func readClaims(r io.Reader) ([]sclaim, error) {
	scanner := bufio.NewScanner(r)
	claims := make([]sclaim, 0)
	var id, x, y, width, length int
	for scanner.Scan() {
		fmt.Sscanf(scanner.Text(), "#%d @ %d,%d: %dx%d", &id, &x, &y, &width, &length)
		claims = append(claims, sclaim{id, x, y, width, length})
	}
	err := scanner.Err()
	return claims, err
}

// Day3Part2 returns the one valid claim
func Day3Part2(r io.Reader) int {
	claims, err := readClaims(r)
	if err != nil {
		log.Fatal(err)
		return -1
	}
	for _, c := range claims {
		stakeClaim(c)
	}

	for _, c := range claims {
		if verifyClaim(c) {
			return c.id
		}
	}
	return -1
}
