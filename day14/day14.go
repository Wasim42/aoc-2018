package day14

import (
	"fmt"
	"reflect"
)

func printScores(scores *[]int, elf1, elf2 int) {
	for i := range *scores {
		if i == elf1 {
			fmt.Printf("(")
		} else if i == elf2 {
			fmt.Printf("[")
		} else {
			fmt.Printf(" ")
		}
		fmt.Printf("%d", (*scores)[i])
		if i == elf1 {
			fmt.Printf(")")
		} else if i == elf2 {
			fmt.Printf("]")
		} else {
			fmt.Printf(" ")
		}
	}
	fmt.Println()
}

// Part1 returns the next 10 digits after runs
func Part1(runs int) string {
	scores := []int{3, 7}
	elf1 := 0
	elf2 := 1

	//printScores(&scores, elf1, elf2)
	for len(scores) < runs+10 {
		result := scores[elf1] + scores[elf2]
		if result < 10 {
			scores = append(scores, result)
		} else {
			scores = append(scores, result/10, result%10)
		}
		elf1 = (elf1 + 1 + scores[elf1]) % (len(scores))
		elf2 = (elf2 + 1 + scores[elf2]) % (len(scores))
		//printScores(&scores, elf1, elf2)
	}

	result := scores[runs : runs+10]
	var answer string
	for _, n := range result {
		answer += fmt.Sprintf("%d", n)
	}

	return answer

}

// Part2 returns the digit at which the passed in score appears
func Part2(wanted []int) int {
	scores := []int{3, 7}
	elf1 := 0
	elf2 := 1

	// Keep going until we find the digits we're looking for
	for {
		result := scores[elf1] + scores[elf2]
		if result < 10 {
			scores = append(scores, result)
		} else {
			scores = append(scores, result/10, result%10)
		}
		elf1 = (elf1 + 1 + scores[elf1]) % (len(scores))
		elf2 = (elf2 + 1 + scores[elf2]) % (len(scores))
		// Check that the last few digits match
		start := len(scores) - len(wanted)
		if start%1000000 == 0 {
			fmt.Println(start, elf1, elf2)
		}
		if start < 1 {
			continue
		}
		if reflect.DeepEqual(scores[start:start+len(wanted)], wanted) {
			return start
		}
		// Sometimes we have one number too many
		start--
		if reflect.DeepEqual(scores[start:start+len(wanted)], wanted) {
			return start
		}
	}
}
