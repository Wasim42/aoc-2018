package day14

import "testing"

type answers struct {
	runs   int
	scores string
}

func TestPart1(t *testing.T) {
	kats := []answers{
		{5, "0124515891"},
		{9, "5158916779"},
		{18, "9251071085"},
		{2018, "5941429882"},
	}
	for _, kat := range kats {
		answer := Part1(kat.runs)
		if answer != kat.scores {
			t.Errorf("FAIL %s != %s for %d runs", answer, kat.scores, kat.runs)
		}
	}
}

type part2Answers struct {
	runs   int
	scores []int
}

func TestPart2(t *testing.T) {
	kats := []part2Answers{
		{9, []int{5, 1, 5, 8, 9}},
		{5, []int{0, 1, 2, 4, 5}},
		{18, []int{9, 2, 5, 1, 0}},
		{2018, []int{5, 9, 4, 1, 4}},
	}
	for _, kat := range kats {
		answer := Part2(kat.scores)
		if answer != kat.runs {
			t.Errorf("FAIL %d != %d for %v score", answer, kat.runs, kat.scores)
		}
	}
}
