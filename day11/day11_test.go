package day11

import (
	"runtime"
	"sync"
	"testing"
)

func TestDay11GetPowerLevel(t *testing.T) {
	kats := [][4]int{
		{3, 5, 8, 4},
		{122, 79, 57, -5},
		{217, 196, 39, 0},
		{101, 153, 71, 4},
	}

	for _, test := range kats {
		var wg sync.WaitGroup
		var answer int
		wg.Add(1)
		expected := test[3]
		go Day11GetPowerLevel(test[0], test[1], test[2], &answer, &wg)
		wg.Wait()
		if answer != expected {
			t.Errorf("Test failed, expected %d, got %d\n", expected, answer)
		}
	}
}

func TestDay11Part1(t *testing.T) {
	kats := [][3]int{
		{18, 33, 45},
		{42, 21, 61},
	}
	for _, test := range kats {
		xx := test[1]
		xy := test[2]
		ax, ay := Day11Part1(test[0])
		if ax != xx || ay != xy {
			t.Errorf("Test failed, expected %d,%d got %d,%d\n", xx, xy, ax, ay)
		}
	}
}

func TestDay11GetTotals(t *testing.T) {
	playBoard := [][]int{
		{1, 2, 3, 4},
		{4, 3, 2, 1},
		{-1, -2, -3, -4},
		{-4, -3, -2, -1},
	}
	expectedTotals := map[int]int{
		1: 1, 2: 10, 3: 9, 4: 0,
	}
	totals := make(chan TotalSize, runtime.NumCPU())
	go Day11GetTotals(&playBoard, 0, 0, totals)
	waiting := 1
	i := 0
	for waiting > 0 {
		cur := <-totals
		if cur.size == 0 {
			waiting--
			continue
		}
		i++
		if cur.total != expectedTotals[cur.size] {
			t.Errorf("Test failed, expected size %d to total %d, got %d\n",
				cur.size, expectedTotals[cur.size], cur.total)
		}
	}
	if i != len(expectedTotals) {
		t.Errorf("Expected %d totals, not %d\n", len(expectedTotals), i)
	}

}
func TestDay11Part2(t *testing.T) {
	kats := [][4]int{
		{18, 90, 269, 16},
		{42, 232, 251, 12},
	}
	for _, test := range kats {
		xx := test[1]
		xy := test[2]
		xsize := test[3]
		ax, ay, asize := Day11Part2(test[0])
		if ax != xx || ay != xy || asize != xsize {
			t.Errorf("Test failed, expected %d,%d,%d got %d,%d,%d\n", xx, xy, xsize, ax, ay, asize)
		}
	}
}
