package day11

import (
	"math"
	"runtime"
	"sync"
)

// Day11GetPowerLevel returns the power level at a point
func Day11GetPowerLevel(x, y, serial int, ret *int, wg *sync.WaitGroup) {
	defer (*wg).Done()
	rackid := x + 10
	power := rackid * y
	power += serial
	power = power * rackid
	level := power % 1000
	*ret = (level / 100) - 5
}

// TotalSize contains the total of the square of size
type TotalSize struct {
	x     int
	y     int
	size  int
	total int
}

// Returns the total of the box on playBoard, starting at position x0,y0,
func getTotal(playBoard *[][]int, x0, y0, length int) (total int) {
	for y := y0; y < y0+length; y++ {
		for x := x0; x < x0+length; x++ {
			total += (*playBoard)[x][y]
		}
	}
	return
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// Day11GetTotals returns all the possible totals of squares starting at
// position x, y, returning them as x, y, size, total
func Day11GetTotals(playBoard *[][]int, startx, starty int, c chan<- TotalSize) {
	totSize := TotalSize{
		x:     startx,
		y:     starty,
		size:  1,
		total: 0,
	}
	sizeX := len(*playBoard)
	sizeY := len((*playBoard)[0])
	// Determine the largest square we can make from here
	maxsize := min(sizeX-startx, sizeY-starty)

	for i := 0; i < maxsize; i++ {
		totSize.size = (1 + i)
		for y := starty; y <= starty+i; y++ {
			value := (*playBoard)[startx+i][y]
			totSize.total += value
		}
		for x := startx; x < startx+i; x++ {
			value := (*playBoard)[x][starty+i]
			totSize.total += value
		}
		c <- totSize
	}
	// Mark that we've finished by pushing an empty TotalSize
	c <- TotalSize{}
}

// This will overwrite the playBoard
func fillBoard(playBoard *[][]int, serial int) {
	var wg sync.WaitGroup
	sizeX := len(*playBoard)
	sizeY := len((*playBoard)[0])
	for y := 0; y < sizeY; y++ {
		for x := 0; x < sizeX; x++ {
			wg.Add(1)
			go Day11GetPowerLevel(x, y, serial, &((*playBoard)[x][y]), &wg)
		}
	}
	wg.Wait()
}

// Day11Part1 returns the largest total power of a 3x3 square
func Day11Part1(serial int) (int, int) {
	runtime.GOMAXPROCS(runtime.NumCPU())

	playBoard := make([][]int, 300)
	for i := range playBoard {
		playBoard[i] = make([]int, 300)
	}
	fillBoard(&playBoard, serial)

	var bigx, bigy, bigtot int
	for y := 0; y < (300 - 3); y++ {
		for x := 0; x < (300 - 3); x++ {
			total := getTotal(&playBoard, x, y, 3)
			if total > bigtot {
				bigtot = total
				bigx = x
				bigy = y
			}
		}
	}
	return bigx, bigy
}

// Day11Part2 returns the location and size of the largest total power of
// any-sized square
func Day11Part2(serial int) (int, int, int) {
	playBoard := make([][]int, 300)
	for i := range playBoard {
		playBoard[i] = make([]int, 300)
	}
	fillBoard(&playBoard, serial)

	totals := make(chan TotalSize, runtime.NumCPU())

	// Kick off a bunch of totalling goroutines on each starting location
	waiting := 0
	for y := 0; y < 300; y++ {
		for x := 0; x < 300; x++ {
			go Day11GetTotals(&playBoard, x, y, totals)
			waiting++
		}
	}

	// The assumption is that we will have at least one positive power.
	// Otherwise perhaps this should be MININT
	biggestPower := TotalSize{total: math.MinInt64}
	// Keep reaping until there are no goroutines to wait for
	for waiting > 0 {
		cur := <-totals
		if cur.size == 0 {
			// One to reap
			waiting--
			continue
		}
		if cur.total < biggestPower.total {
			continue
		}
		biggestPower.x = cur.x
		biggestPower.y = cur.y
		biggestPower.size = cur.size
		biggestPower.total = cur.total
	}

	return biggestPower.x, biggestPower.y, biggestPower.size
}
