package day19

import (
	"bufio"
	"fmt"
	"io"
)

const (
	a = 0
	b = 1
	c = 2
)

type register []int
type operation []int

var ip int // The Instruction Pointer
var ipReg int

// Sets which register manipulates the Instruction Pointer
func setIPReg(reg int) {
	ipReg = reg
}

// Sets the right register to the value in IP
func preIP(r register) register {
	r[ipReg] = ip
	return r
}

// Sets the IP from the right register, then increments it
func postIP(r register) {
	ip = r[ipReg]
	ip++
}

func addr(before register, op operation) register {
	after := preIP(before)
	after[op[c]] = before[op[a]] + before[op[b]]
	postIP(after)
	return after
}

func addi(before register, op operation) register {
	after := preIP(before)
	after[op[c]] = before[op[a]] + op[b]
	postIP(after)
	return after
}

func mulr(before register, op operation) register {
	after := preIP(before)
	after[op[c]] = before[op[a]] * before[op[b]]
	postIP(after)
	return after
}

func muli(before register, op operation) register {
	after := preIP(before)
	after[op[c]] = before[op[a]] * op[b]
	postIP(after)
	return after
}

func banr(before register, op operation) register {
	after := preIP(before)
	after[op[c]] = before[op[a]] & before[op[b]]
	postIP(after)
	return after
}

func bani(before register, op operation) register {
	after := preIP(before)
	after[op[c]] = before[op[a]] & op[b]
	postIP(after)
	return after
}

func borr(before register, op operation) register {
	after := preIP(before)
	after[op[c]] = before[op[a]] | before[op[b]]
	postIP(after)
	return after
}

func bori(before register, op operation) register {
	after := preIP(before)
	after[op[c]] = before[op[a]] | op[b]
	postIP(after)
	return after
}

func setr(before register, op operation) register {
	after := preIP(before)
	after[op[c]] = before[op[a]]
	postIP(after)
	return after
}

func seti(before register, op operation) register {
	after := preIP(before)
	//fmt.Printf("\n\n%v seti %v\n", after, op)
	after[op[c]] = op[a]
	postIP(after)
	return after
}

func gtir(before register, op operation) register {
	after := preIP(before)
	if op[a] > before[op[b]] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	postIP(after)
	return after
}

func gtri(before register, op operation) register {
	after := preIP(before)
	if before[op[a]] > op[b] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	postIP(after)
	return after
}

func gtrr(before register, op operation) register {
	after := preIP(before)
	if before[op[a]] > before[op[b]] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	postIP(after)
	return after
}

func eqir(before register, op operation) register {
	after := preIP(before)
	if op[a] == before[op[b]] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	postIP(after)
	return after
}

func eqri(before register, op operation) register {
	after := preIP(before)
	if before[op[a]] == op[b] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	postIP(after)
	return after
}

func eqrr(before register, op operation) register {
	after := preIP(before)
	if before[op[a]] == before[op[b]] {
		after[op[c]] = 1
	} else {
		after[op[c]] = 0
	}
	postIP(after)
	return after
}

type fmap map[string]func(register, operation) register

func initFP() fmap {
	funky := make(fmap)
	// regex ftw!
	funky["addr"] = addr
	funky["addi"] = addi
	funky["mulr"] = mulr
	funky["muli"] = muli
	funky["banr"] = banr
	funky["bani"] = bani
	funky["borr"] = borr
	funky["bori"] = bori
	funky["setr"] = setr
	funky["seti"] = seti
	funky["gtir"] = gtir
	funky["gtri"] = gtri
	funky["gtrr"] = gtrr
	funky["eqir"] = eqir
	funky["eqri"] = eqri
	funky["eqrr"] = eqrr
	return funky
}

type instruction struct {
	op   string
	code operation
}

func parseInstructions(r io.Reader) (todo []instruction) {
	scanner := bufio.NewScanner(r)
	scanner.Scan()
	// The first line tells us the Instruction Pointer register
	ipr := 0
	fmt.Sscanf(scanner.Text(), "#ip %d", &ipr)
	setIPReg(ipr)

	// Now we're onto the main course...
	for scanner.Scan() {
		ins := instruction{"broken", operation{0, 0, 0}}
		fmt.Sscanf(scanner.Text(), "%s %d %d %d", &(ins.op),
			&(ins.code[0]), &(ins.code[1]), &(ins.code[2]))
		todo = append(todo, ins)
	}
	return todo
}

var debug bool

// SetDebug sets the debug value
func SetDebug(d bool) {
	debug = d
}

// Part1 returns the value of register 0
func Part1(r io.Reader) int {
	funky := initFP()
	todo := parseInstructions(r)

	reg := register{0, 0, 0, 0, 0, 0}
	for ip < len(todo) && ip >= 0 {
		if debug {
			fmt.Printf("ip=%d %v %s %v ", ip, reg, todo[ip].op, todo[ip].code)
		}
		reg = funky[todo[ip].op](reg, todo[ip].code)
		if debug {
			fmt.Printf("%v\n", reg)
		}
	}
	return reg[0]
}

// Part2 returns the value of register 0
func Part2(r io.Reader) string {
	return "This is all a trick - watch a video that explains it!"
	// funky := initFP()
	// todo := parseInstructions(r)

	// reg := register{1, 0, 0, 0, 0, 0}
	// for ip < len(todo) && ip >= 0 {
	// 	if debug {
	// 		fmt.Printf("ip=%d %v %s %v ", ip, reg, todo[ip].op, todo[ip].code)
	// 	}
	// 	reg = funky[todo[ip].op](reg, todo[ip].code)
	// 	if debug {
	// 		fmt.Printf("%v\n", reg)
	// 	}
	// }
	// return reg[0]
}
