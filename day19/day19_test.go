package day19

import (
	"strings"
	"testing"
)

func TestPart1(t *testing.T) {
	input := strings.NewReader(`#ip 0
seti 5 0 1
seti 6 0 2
addi 0 1 0
addr 1 2 3
setr 1 0 0
seti 8 0 4
seti 9 0 5`)
	expected := 6
	SetDebug(true)
	answer := Part1(input)
	if answer != expected {
		t.Errorf("Expected: %d, but got %d instead\n", expected, answer)
	}
}
