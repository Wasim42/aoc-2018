package day13

import (
	"bufio"
	"fmt"
	"io"
	"time"

	"github.com/fatih/color"
)

var mineTrack [][]rune

var carts map[Location]mineCraft

// Location is an x,y co-ordinate
type Location struct {
	x int
	y int
}

// mineCrafts are carts
type mineCraft struct {
	direction orientation
	decision  turning
}

type turning int

const (
	left turning = iota
	straight
	right
)

func (t *turning) next() {
	*t = (*t + 1) % 3
}

func (cart *mineCraft) decide() {
	switch cart.decision {
	case left:
		switch cart.direction {
		case '>':
			cart.direction = '^'
		case '^':
			cart.direction = '<'
		case '<':
			cart.direction = 'v'
		case 'v':
			cart.direction = '>'
		}
	case right:
		switch cart.direction {
		case '>':
			cart.direction = 'v'
		case '^':
			cart.direction = '>'
		case '<':
			cart.direction = '^'
		case 'v':
			cart.direction = '<'
		}
	}
	cart.decision.next()
}

type orientation rune

// Given an existing direction, and a turning, this returns a new direction
func (direction *orientation) reOrient(turning rune) {
	switch turning {
	case '/':
		switch *direction {
		case '>':
			*direction = '^'
		case '^':
			*direction = '>'
		case '<':
			*direction = 'v'
		case 'v':
			*direction = '<'
		}
	case '\\': // \
		switch *direction {
		case '>':
			*direction = 'v'
		case '^':
			*direction = '<'
		case '<':
			*direction = '^'
		case 'v':
			*direction = '>'
		}
	}
}

func (cart *mineCraft) move(loc Location) Location {
	switch cart.direction {
	case '>':
		loc.x++
	case '<':
		loc.x--
	case '^':
		loc.y--
	case 'v':
		loc.y++
	}
	if mineTrack[loc.y][loc.x] == '/' || mineTrack[loc.y][loc.x] == '\\' {
		cart.direction.reOrient(mineTrack[loc.y][loc.x])
	}
	if mineTrack[loc.y][loc.x] == '+' {
		cart.decide()
	}
	return loc
}

func parseTrack(r io.Reader) {
	// Initialise
	mineTrack = make([][]rune, 0)
	carts = make(map[Location]mineCraft)

	scanner := bufio.NewScanner(r)

	for y := 0; scanner.Scan(); y++ {
		if len(scanner.Text()) == 0 {
			// A blank line, so skip it
			y--
			continue
		}
		newRow := make([]rune, len(scanner.Text()))

		for x, c := range scanner.Text() {
			if c != '<' && c != '>' && c != '^' && c != 'v' {
				// Not a cart, just add it
				newRow[x] = c
				continue
			}
			// We have a cart, so which orientation?
			if c == '<' || c == '>' {
				newRow[x] = '-'
			} else {
				newRow[x] = '|'
			}
			newLoc := Location{x, y}
			newCart := mineCraft{direction: orientation(c)}
			carts[newLoc] = newCart
		}
		mineTrack = append(mineTrack, newRow)
	}
}

func printMine() {
	for y := range mineTrack {
		for x := range mineTrack[y] {
			loc := Location{x, y}
			cart, ok := carts[loc]
			c := color.New(color.FgBlue)
			if ok {
				c.Add(color.Bold)
				c.Add(color.FgBlue)
				if cart.direction == 'X' {
					c = c.Add(color.FgRed)
				} else {
					c.Add(color.BgWhite)
				}
				c.Printf("%c", cart.direction)
				continue
			}
			c.Printf("%c", mineTrack[y][x])
		}
		fmt.Println()
	}
}

func tick(del bool) bool {
	noCrash := true
	newCarts := make(map[Location]mineCraft)

	for y := range mineTrack {
		for x := range mineTrack[y] {
			loc := Location{x, y}
			cart, ok := carts[loc]
			if !ok {
				// No cart, ignore it
				continue
			}
			newLoc := cart.move(loc)
			_, ok = carts[newLoc]
			if ok {
				// Crashed into an old cart
				noCrash = false
				cart.direction = 'X'
				if del {
					delete(carts, newLoc)
					continue
				}
			}
			_, ok = newCarts[newLoc]
			if ok {
				// Crashed into a new cart
				noCrash = false
				cart.direction = 'X'
				if del {
					delete(newCarts, newLoc)
					continue
				}
			}
			delete(carts, loc)
			newCarts[newLoc] = cart
		}
	}
	carts = newCarts
	return noCrash
}

// Part1 returns the co-ordinates of the crash site
func Part1(r io.Reader, debug bool) string {
	parseTrack(r)
	if debug {
		printMine()
	}
	for t := 1; tick(false); t++ {
		if debug {
			printMine()
			fmt.Println("Tick:", t)
			time.Sleep(100 * time.Millisecond)
		}
	}
	if debug {
		printMine()
	}
	// Find the crash site
	for loc, cart := range carts {
		if cart.direction == 'X' {
			return fmt.Sprintf("%d,%d", loc.x, loc.y)
		}
	}
	return fmt.Sprintf("%d,%d", -1, -1)
}

// Part2 returns the co-ordinates of the last cart
func Part2(r io.Reader, debug bool) string {
	parseTrack(r)
	if debug {
		printMine()
	}
	for len(carts) > 1 {
		tick(true)
		if debug {
			printMine()
			time.Sleep(100 * time.Millisecond)
		}
	}
	if debug {
		printMine()
	}
	// Find the crash site
	for loc := range carts {
		return fmt.Sprintf("%d,%d", loc.x, loc.y)
	}
	return fmt.Sprintf("%d,%d", -1, -1)
}
