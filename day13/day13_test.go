package day13

import (
	"io"
	"strings"
	"testing"
)

type questionAnswers struct {
	input  io.Reader
	answer string
}

func TestDay13Part1(t *testing.T) {
	kats := []questionAnswers{
		{strings.NewReader(`
/->-\        
|   |  /----\
| /-+--+-\  |
| | |  | v  |
\-+-/  \-+--/
  \------/   
`), "7,3"},
		// 		{strings.NewReader(`
		// /<>----------------------------\
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// |                              |
		// \------------------------------/
		// `), "30,16"},
		{strings.NewReader(`
-->>--`), "3,0"},
		{strings.NewReader(`
|
v
v
|`), "0,2"},
	}

	for _, kat := range kats {
		input := kat.input
		expected := kat.answer
		answer := Part1(input, true)
		if answer != expected {
			t.Errorf("Expected (%s), got (%s)", expected, answer)
		}
	}

}

func TestPart2(t *testing.T) {
	input := strings.NewReader(`
/>-<\  
|   |  
| /<+-\
| | | v
\>+</ |
  |   ^
  \<->/`)
	expected := "6,4"

	answer := Part2(input, true)
	if answer != expected {
		t.Errorf("Expected (%s), got (%s)", expected, answer)
	}
}
