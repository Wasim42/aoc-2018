package main

import (
	"advent/day1"
	"advent/day2"
	"advent/day20"
	"advent/day3"
	"advent/day4"
	"advent/day6"
	"advent/day7"
	"advent/day8"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime/pprof"
)

func open(fname string) io.Reader {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}
	return f
}

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	//var x, y, size int
	fmt.Println("Day 1 Part 1: ", day1.Day1Part1(open("day1/day1.txt")))
	fmt.Println("Day 1 Part 2: ", day1.Day1Part2(open("day1/day1.txt")))
	fmt.Println("Day 2 Part 1: ", day2.Day2Part1(open("day2/day2.txt")))
	fmt.Println("Day 2 Part 2: ", day2.Day2Part2(open("day2/day2.txt")))
	fmt.Println("Day 3 Part 1: ", day3.Day3Part1(open("day3/day3.txt")))
	fmt.Println("Day 3 Part 2: ", day3.Day3Part2(open("day3/day3.txt")))
	fmt.Println("Day 4 Part 1: ", day4.Day4Part1(open("day4/day4.txt")))
	fmt.Println("Day 4 Part 2: ", day4.Day4Part2())
	fmt.Println("Skipping day 5")
	//fmt.Println("Day 5 Part 1: ", day5.Day5Part1(open("day5/day5.txt")))
	//fmt.Println("Day 5 Part 2: ", day5.Day5Part2(open("day5/day5.txt")))
	fmt.Println("Day 6 Part 1: ", day6.Day6Part1(open("day6/day6.txt")))
	fmt.Println("Day 6 Part 2: ", day6.Day6Part2(open("day6/day6.txt"), 10000))
	fmt.Println("Day 7 Part 1: ", day7.Day7Part1(open("day7/day7.txt")))
	fmt.Println("Day 7 Part 2: ", day7.Day7Part2(open("day7/day7.txt"), 5, 60))
	fmt.Println("Day 8 Part 1: ", day8.Day8Part1(open("day8/day8.txt")))
	fmt.Println("Day 8 Part 2: ", day8.Day8Part2(open("day8/day8.txt")))
	fmt.Println("Skipping Day 9")
	//fmt.Println("Day 9 Part 1: ", day9.Day9Part1(452, 71250))
	//fmt.Println("Day 9 Part 2: ", day9.Day9Part1(452, 7125000))
	fmt.Println("Skipping Day 10")
	//fmt.Println("Day 10 Part 1: ", day10.Day10Part1(open("day10/day10.txt")))
	fmt.Println("Skipping Day 11")
	//x, y = day11.Day11Part1(5093)
	//fmt.Println("Day 11 Part 1: ", x, y)
	//x, y, size = day11.Day11Part2(5093)
	//fmt.Println("Day 11 Part 2: ", x, y, size)
	fmt.Println("Skipping Day 12")
	//fmt.Println("Day 12 Part 1: ", day12.Day12Part1(open("day12/day12.txt"), 20))
	//fmt.Println("Day 12 Part 2: ", day12.Day12Part1(open("day12/day12.txt"), 50000000000))
	fmt.Println("Skipping Day 13")
	//fmt.Println("Day 13 Part 1: ", day13.Part1(open("day13/day13.txt"), false))
	//fmt.Println("Day 13 Part 2: ", day13.Part2(open("day13/day13.txt"), false))
	fmt.Println("Skipping Day 14")
	//fmt.Println("Day 14 Part 1: ", day14.Part1(793031))
	//fmt.Println("Day 14 Part 1: ", day14.Part2([]int{7, 9, 3, 0, 3, 1}))
	fmt.Println("Skipping Day 15")
	//fmt.Println("Day 15 Part 1: ", day15.Part1(open("day15/day15.txt")))
	//fmt.Println("Day 15 Part 2: ", day15.Part2(open("day15/day15.txt")))
	fmt.Println("Skipping Day 16")
	//fmt.Println("Day 16 Part 1: ", day16.Part1(open("day16/day16.txt")))
	//fmt.Println("Day 16 Part 2: ", day16.Part2(open("day16/day16.txt")))
	fmt.Println("Skipping Day 17")
	//fmt.Println("Day 17 Part 1: ", day17.Part1(open("day17/day17.txt")))
	//fmt.Println("Day 17 Part 2: ", day17.Part2(open("day17/day17.txt")))
	fmt.Println("Skipping Day 18")
	//fmt.Println("Day 18 Part 1: ", day18.Part1(open("day18/day18.txt")))
	//fmt.Println("Day 18 Part 2: ", day18.Part2(open("day18/day18.txt")))
	fmt.Println("Skipping Day 19")
	//fmt.Println("Day 19 Part 1:", day19.Part1(open("day19/day19.txt")))
	//day19.SetDebug(true)
	//fmt.Println("Day 19 Part 2:", day19.Part2(open("day19/day19.txt")))
	day20.SetDoDot(true)
	fmt.Println("Day 20 Part 1:", day20.Part1(open("day20/day20.txt")))
}
