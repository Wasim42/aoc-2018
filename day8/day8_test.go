package day8

import (
	"strings"
	"testing"
)

func TestDay8Part1(t *testing.T) {
	testInput := strings.NewReader("2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2")

	answer := Day8Part1(testInput)
	expected := 138

	if answer != expected {
		t.Errorf("Test failed, expected %d, got %d\n", expected, answer)
	}

}

func TestDay8Part2(t *testing.T) {
	testInput := strings.NewReader("2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2")

	answer := Day8Part2(testInput)
	expected := 66

	if answer != expected {
		t.Errorf("Test failed, expected %d, got %d\n", expected, answer)
	}

}
