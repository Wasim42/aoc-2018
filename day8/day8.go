package day8

import (
	"bufio"
	"fmt"
	"io"
)

// The Tree struct that contains the data held in the file
type Tree struct {
	name     rune
	metadata []int
	children []Tree
}

func parseNode2(scanner *bufio.Scanner, r rune) (tree Tree) {
	tree.name = r
	r++
	for scanner.Scan() {
		var children, metadata int
		fmt.Sscanf(scanner.Text(), "%d", &children)
		scanner.Scan()
		fmt.Sscanf(scanner.Text(), "%d", &metadata)
		fmt.Printf(" C%d M%d", children, metadata)

		for c := 0; c < children; c++ {
			tree.children = append(tree.children, parseNode2(scanner, r))
		}
		for m := 0; m < metadata; m++ {
			scanner.Scan()
			var md int
			fmt.Sscanf(scanner.Text(), "%d", &md)
			fmt.Printf(" m%d", md)
			tree.metadata = append(tree.metadata, md)
		}
	}
	return
}

func parseNode(scanner *bufio.Scanner, r rune) (Tree, rune) {
	var tree Tree
	tree.name = r
	r++

	scanner.Scan()
	var numC, numM int
	fmt.Sscanf(scanner.Text(), "%d", &numC)
	scanner.Scan()
	fmt.Sscanf(scanner.Text(), "%d", &numM)
	children := make([]Tree, 0, numC)
	for c := 0; c < numC; c++ {
		var child Tree
		child, r = parseNode(scanner, r)
		children = append(children, child)
	}

	metadata := make([]int, 0, numM)
	for m := 0; m < numM; m++ {
		scanner.Scan()
		var md int
		fmt.Sscanf(scanner.Text(), "%d", &md)
		metadata = append(metadata, md)
	}
	tree.children = children
	tree.metadata = metadata

	return tree, r
}

func parseTree(r io.Reader) Tree {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanWords)

	tree, _ := parseNode(scanner, 'A')
	return tree
}

func totalMetaData(tree Tree) int {
	total := 0
	for _, md := range tree.metadata {
		total += md
	}
	for _, c := range tree.children {
		total += totalMetaData(c)
	}
	return total
}

func funnyMD(tree Tree) (total int) {
	// If a node has no child nodes, its value is the sum of its metadata entries.
	if len(tree.children) == 0 {
		for _, m := range tree.metadata {
			total += m
		}
		return
	}
	// However, if a node does have child nodes, the metadata entries become
	// indexes which refer to those child nodes.
	for _, m := range tree.metadata {
		if m > len(tree.children) {
			continue
		}
		// Note the non-zero indexing
		total += funnyMD(tree.children[m-1])
	}
	return
}

func printTree(tree Tree) {
	fmt.Printf(" %c => ", tree.name)
	for _, m := range tree.metadata {
		fmt.Printf(" %d ", m)
	}
	fmt.Println()
	for _, c := range tree.children {
		printTree(c)
	}
}

// Day8Part1 reads a file and turns it into a tree, then sums all the metadata
func Day8Part1(r io.Reader) int {
	tree := parseTree(r)
	//printTree(tree)
	return totalMetaData(tree)
}

// Day8Part2 reads a file and turns it into a tree, then sums all the metadata
func Day8Part2(r io.Reader) int {
	tree := parseTree(r)
	//printTree(tree)
	return funnyMD(tree)
}
