package day20

import (
	"fmt"
	"strings"
	"testing"
)

type testParam struct {
	input  string
	answer int
}

func TestPart1(t *testing.T) {
	SetVerbose(true)
	SetDoDot(true)
	tests := []testParam{
		{"^WNE$", 3},
		{"^WNES$", 2},
		{"^N(E|W)N$", 3},
		{"^N(E|W)N(E|)N$", 5},
		{"^N(E|(W|N))N(E|)N$", 5},
		{"^N(E|(WN|)SSS)$", 4},
		{"^ENWWW(NEEE|SSE(EE|N))$", 10},
		{"^EEE(NEEE|)NN$", 9},
		{"^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$", 18},
		{"^E(N(NNN|EEE)ES|)NN$", 7},
		{"^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$", 23},
		{"^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$", 31},
	}

	for _, tp := range tests {
		answer := Part1(strings.NewReader(tp.input))
		if answer != tp.answer {
			t.Errorf("Expected %d, got %d instead", tp.answer, answer)
		} else {
			fmt.Println("Passed", tp.input)
		}
	}
}
