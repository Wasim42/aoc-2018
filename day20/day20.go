package day20

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"time"
)

var verbose bool

// SetVerbose sets the verbosity
func SetVerbose(tothis bool) {
	verbose = tothis
}

// Queue is a queue
type Queue []interface{}

func (q *Queue) push(item interface{}) {
	if *q == nil {
		*q = make(Queue, 0)
	}
	*q = append(*q, item)
}

func (q *Queue) pop() interface{} {
	if q.isEmpty() {
		return nil
	}
	item := (*q)[0]
	*q = (*q)[1:]
	return item
}

func (q *Queue) isEmpty() bool {
	return *q == nil || len(*q) == 0
}

// Stack is a stack
type Stack []interface{}

func (s *Stack) push(item interface{}) {
	if *s == nil {
		*s = make(Stack, 0)
	}
	*s = append(*s, item)
}

func (s *Stack) peek() interface{} {
	if s.isEmpty() {
		return nil
	}
	return (*s)[len(*s)-1]
}

func (s *Stack) pop() interface{} {
	if s.isEmpty() {
		return nil
	}
	item := (*s)[len(*s)-1]
	*s = (*s)[0 : len(*s)-1]
	return item
}

func (s *Stack) isEmpty() bool {
	if *s == nil || len(*s) == 0 {
		return true
	}
	return false
}

// Location has x and y co-ordinates
// Origin is at X
// North and West are positive
type Location struct {
	x int
	y int
}

// Room cardinal points are true if a door, false if a wall
type Room struct {
	loc   Location
	north bool
	east  bool
	south bool
	west  bool
}

// Branch holds a branching position
type Branch struct {
	loc      Location // Current location
	pos      int      // Position in the string
	movement string   // Which direction we went through to get here
}

func printMaze(maze map[Location]Room, input string) {
	// First, let's get the bounding box
	minx, maxx, miny, maxy := 0, 0, 0, 0

	for l := range maze {
		if l.x < minx {
			minx = l.x
		}
		if l.x > maxx {
			maxx = l.x
		}
		if l.y < miny {
			miny = l.y
		}
		if l.y > maxy {
			maxy = l.y
		}
	}

	fmt.Println(input)

	for y := maxy; y >= miny; y-- {
		// Note that there are two levels to each room - the join, and then the
		// room (there's also the bottom row which we'll just treat as a line
		// of walls)
		for i := 0; i < 2; i++ {
			for x := minx; x <= maxx; x++ {
				l := Location{x, y}
				r := maze[l]
				if i == 0 {
					// Each corner is a wall
					fmt.Printf("#")
					if r.north {
						fmt.Printf("-")
					} else {
						fmt.Printf("#")
					}
					if x == maxx {
						fmt.Printf("#")
					}
					continue
				}
				// Room level: W.EW.EW.EW.EW.EW.E
				if r.west {
					fmt.Printf("|")
				} else {
					fmt.Printf("#")
				}
				if x == 0 && y == 0 {
					fmt.Printf("X")
				} else {
					fmt.Printf(".")
				}
				if x < maxx {
					continue
				}
				// The end of the line
				if r.east {
					fmt.Printf("|")
				} else {
					fmt.Printf("#")
				}
			}
			fmt.Println()
		}
	}
	// The last line of hashes
	for x := minx; x <= maxx; x++ {
		fmt.Printf("##")
	}
	fmt.Printf("#")
	fmt.Println()
}

// Tree is a tree
type Tree struct {
	Segment  string
	children []*Tree
}

// AddChild adds a child to the tree
func (t *Tree) AddChild() *Tree {
	// By now we know the segment data of the parent, so set that, and the
	// relationship to that of the child
	if len(t.children) < 1 {
		gvfile.WriteString(strings.Replace(fmt.Sprintf("%p [label=\"%s\"]\n", t, t.Segment), "0x", "", -1))
	}
	child := Tree{}
	gvfile.WriteString(strings.Replace(fmt.Sprintf("%p -> %p\n", t, &child), "0x", "", -1))
	t.children = append(t.children, &child)
	return &child
}

func inheritor(c *Tree) *Tree {
	if len(c.children) < 1 {
		return c
	}
	return inheritor(c.children[0])
}

// JoinToLeaves goes through all the root nodes of this node, and adds the same
// child
func (t *Tree) JoinToLeaves() *Tree {
	child := Tree{}

	for _, c := range t.children {
		// For each of these children of the node, we only need to traverse
		// down the first child... ultimately they will join together (where
		// they don't is where we're going to put the join in anyway)
		i := inheritor(c)
		if len(i.children) < 1 {
			gvfile.WriteString(strings.Replace(fmt.Sprintf("%p [label=\"%s\"]\n", i, i.Segment), "0x", "", -1))
		}
		i.children = append(i.children, &child)
		gvfile.WriteString(strings.Replace(fmt.Sprintf("%p -> %p\n", i, &child), "0x", "", -1))
	}
	return &child
}

type nodeTraversalData struct {
	node   *Tree
	voyage string
}

var doDot bool

// SetDoDot permits dot-file writing
func SetDoDot(tothis bool) {
	doDot = tothis
}

var gvfile *os.File

func buildMaze(input string) map[Location]Room {

	if doDot {
		var err error
		fname := fmt.Sprintf("%d.gv", time.Now().UnixNano())
		gvfile, err = os.Create(fname)
		if err != nil {
			panic("Uname to open dot file")
		}
		defer gvfile.Close()

		header := fmt.Sprintf("digraph \"%s\" {\n", input)
		gvfile.WriteString(header)
	}

	tctree := Tree{}
	node := &tctree
	var parent *Tree
	// depth stores the tree at which we branch (that holds the open bracket)
	depth := make([]*Tree, 0)
	for _, c := range input {
		switch c {
		case '^':
			// Start of our journey
			node.Segment = "^"
			node = node.AddChild()
		case '$':
			// This is the end
			fmt.Println(node)
		case 'N', 'E', 'W', 'S':
			node.Segment = fmt.Sprintf("%s%c", node.Segment, c)
		case '(':
			if verbose {
				fmt.Println("Segment:", node.Segment)
			}
			node = node.AddChild()
			node.Segment = "("
			depth = append(depth, node)
			node = node.AddChild()
		case '|':
			parent := depth[len(depth)-1]
			node = parent.AddChild()
		case ')':
			parent, depth = depth[len(depth)-1], depth[:len(depth)-1]
			// We now need to go to the bottom children of the parent, and join
			// them altogether
			node = parent.JoinToLeaves()
			node.Segment = ")"
		}

	}
	if doDot {
		gvfile.WriteString("}\n")
	}

	// We now need to turn the tree into a maze
	maze := make(map[Location]Room)

	// We start at (0,0) at the start of the regex, so set that up
	maze[Location{0, 0}] = Room{loc: Location{0, 0}}

	// Now we need to traverse the tree

	// Use a recursive algorithm to go through the tree, always adding to the
	// global maze

	return *traverse(&tctree, &maze, Location{0, 0})
}

func traverse(node *Tree, maze *map[Location]Room, curLocation Location) *map[Location]Room {
	getNewRoom := func(maze *map[Location]Room, newPos Location) Room {
		newRoom, ok := (*maze)[newPos]
		if !ok {
			newRoom = Room{loc: newPos}
		}
		return newRoom
	}
	// Parse through the segment of this maze
	curRoom := getNewRoom(maze, curLocation)
	x := curLocation.x
	y := curLocation.y
	for _, c := range node.Segment {
		curPos := Location{x, y}
		switch c {
		case 'N':
			y++
		case 'S':
			y--
		case 'E':
			x++
		case 'W':
			x--
		}
		newPos := Location{x, y}
		newRoom := getNewRoom(maze, newPos)
		joinRoom(&curRoom, &newRoom, c)
		(*maze)[curPos] = curRoom
		(*maze)[newPos] = newRoom
		curRoom = newRoom
		curLocation = newPos
	}
	// Now process the children before returning our maze
	for _, k := range node.children {
		maze = traverse(k, maze, curLocation)
	}
	return maze
}

//	ntds := Stack{}
//	ntds.push(nodeTraversalData{node: &tctree})
//
//	numvoyages := 0
//
//	for !ntds.isEmpty() {
//		ntd := ntds.pop().(nodeTraversalData)
//		node := ntd.node
//		voyage := ntd.voyage
//
//		// The journey so far...
//		if node.Segment != "(" {
//			voyage = voyage + node.Segment
//		}
//
//		// More to come?
//		if len(node.children) > 0 {
//			// Add the children's journeys to the stack, and then we're done
//			for _, child := range node.children {
//				ntds.push(nodeTraversalData{child, voyage})
//			}
//			continue
//		}
//
//		// A voyage is now complete - let's deal with it
//		if verbose {
//			ok := matcher.MatchString(voyage)
//			if !ok {
//				fmt.Printf("%s does not match!\n", voyage)
//			}
//		}
//		numvoyages++
//		if numvoyages%20000 == 0 {
//			fmt.Print(numvoyages, " ")
//		}
//
//		// Each of these journeys start at (0,0)
//		curRoom := getNewRoom(maze, Location{0, 0})
//		x := 0
//		y := 0
//		for _, c := range voyage {
//			curPos := Location{x, y}
//			switch c {
//			case 'N':
//				y++
//			case 'S':
//				y--
//			case 'E':
//				x++
//			case 'W':
//				x--
//			}
//			newPos := Location{x, y}
//			newRoom := getNewRoom(maze, newPos)
//			joinRoom(&curRoom, &newRoom, c)
//			maze[curPos] = curRoom
//			maze[newPos] = newRoom
//			curRoom = newRoom
//		}
//	}
//
//	if verbose {
//		printMaze(maze, input)
//	}
//
//	return maze
//}

func joinRoom(curRoom, newRoom *Room, dir rune) {
	switch dir {
	case 'N':
		curRoom.north = true
		newRoom.south = true
	case 'S':
		curRoom.south = true
		newRoom.north = true
	case 'E':
		curRoom.east = true
		newRoom.west = true
	case 'W':
		curRoom.west = true
		newRoom.east = true
	}
}

type bfsNode struct {
	loc  Location
	dist int
}

// longestDistance returns the longest distance from the origin
func longestDistance(maze map[Location]Room) (furthest int) {
	q := Queue{}
	// push our origin
	q.push(bfsNode{Location{0, 0}, 0})

	visited := map[Location]bool{}
	for !q.isEmpty() {
		r := q.pop().(bfsNode)
		if visited[r.loc] {
			// Already seen this room - ignore it
			continue
		}
		if r.dist > furthest {
			furthest = r.dist
		}
		room := maze[r.loc]
		visited[room.loc] = true
		r.dist++
		if room.north {
			q.push(bfsNode{Location{room.loc.x, room.loc.y + 1}, r.dist})
		}
		if room.east {
			q.push(bfsNode{Location{room.loc.x + 1, room.loc.y}, r.dist})
		}
		if room.south {
			q.push(bfsNode{Location{room.loc.x, room.loc.y - 1}, r.dist})
		}
		if room.west {
			q.push(bfsNode{Location{room.loc.x - 1, room.loc.y}, r.dist})
		}
	}
	return
}

// Part1 returns the shortest number of steps to the furthest door
func Part1(r io.Reader) int {
	// The input is one *very* long line
	scanner := bufio.NewScanner(r)
	scanner.Scan()
	input := scanner.Text()
	maze := buildMaze(input)
	return longestDistance(maze)
}
