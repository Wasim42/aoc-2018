package day9

import (
	"container/ring"
	"fmt"
)

func printBoard(curPlayer int, circle, curMarble *ring.Ring) {
	fmt.Printf("[%d] ", curPlayer)
	i := circle
	for {
		if i == curMarble {
			fmt.Printf(" (%2d)", i.Value)
		} else {
			fmt.Printf("  %2d ", i.Value)
		}
		i = i.Next()
		if i == circle {
			break
		}
	}
	fmt.Println()
}

// Day9Part1 returns the score
func Day9Part1(numPlayers, lastMarble int) int {
	circle := ring.New(1)
	circle.Value = 0
	curMarble := circle
	curPlayer := 0
	// 1st player will be player 1, so player zero is empty
	scores := make([]int, numPlayers+1)
	// printBoard(curPlayer, circle, curMarble)
	for placeMarble := 1; placeMarble <= lastMarble; placeMarble++ {
		if placeMarble%23 == 0 {
			// The scoring case
			scores[curPlayer] += placeMarble
			// Now take out the number 7 places to the left, and add it to the
			// score
			takeout := curMarble
			for i := 0; i < 7; i++ {
				takeout = takeout.Prev()
			}
			scores[curPlayer] += takeout.Value.(int)
			curMarble = takeout.Next()
			takeout.Prev().Unlink(1)

		} else {
			// The normal case
			// Place the new marble into position 2 positions along from curMarble
			var newPos *ring.Ring
			if placeMarble < 3 {
				newPos = circle
			} else {
				newPos = curMarble.Next()
			}
			newMarble := ring.New(1)
			newMarble.Value = placeMarble
			newPos.Link(newMarble)
			curMarble = newMarble
		}
		// printBoard(curPlayer, circle, curMarble)
		// Reset for the next round
		curPlayer++
		if curPlayer > numPlayers {
			curPlayer = 1
		}
	}
	// We need to return the highest score
	total := 0
	for _, i := range scores {
		if i > total {
			total = i
		}
	}
	return total
}
