package day9

import "testing"

func TestDay9Part1(t *testing.T) {
	kats := [][3]int{
		{9, 25, 32},
		{10, 1618, 8317},
		{13, 7999, 146373},
		{17, 1104, 2764},
		{21, 6111, 54718},
		{30, 5807, 37305},
	}

	for _, k := range kats {
		expected := k[2]
		answer := Day9Part1(k[0], k[1])
		if answer != expected {
			t.Errorf("Test failed, expected %d, got %d\n", expected, answer)
		}
	}

}
