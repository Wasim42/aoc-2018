package day4

import (
	"bufio"
	"io"
	"log"
	"regexp"
	"sort"
	"strconv"
	"time"
)

// Sleep holds one individual sleep datum
type Sleep struct {
	start time.Time // The first minute of sleep
	stop  time.Time // The first minute of woke
}

// Guards is a map by guard id, containing a number of sleeps
var Guards map[int][]Sleep

// This fills up the Guards map
func parseGuardSleeps(r io.Reader) {
	// We need to initialise Guards before filling it in
	Guards = make(map[int][]Sleep, 0)

	// Step 1: sort the input file by date

	actions := make(map[time.Time]string, 0)

	scanner := bufio.NewScanner(r)
	// Format is [datestr] Action
	matcher := regexp.MustCompile(`\[(.*)\] (.*)`)
	for scanner.Scan() {
		matches := matcher.FindStringSubmatch(scanner.Text())
		// The last line doesn't match
		if len(matches) == 3 {
			datestr := matches[1]
			action := matches[2]
			t, err := time.Parse("2006-01-02 15:04", datestr)
			if err != nil {
				log.Fatal(err)
				return
			}
			actions[t] = action
		}
	}

	// Sort the dates
	var keys []time.Time
	for k := range actions {
		keys = append(keys, k)
	}
	sort.Slice(keys, func(i, j int) bool {
		return keys[i].Before(keys[j])
	})

	// Now, we need to parse the inputs by guard...
	shift := regexp.MustCompile(`Guard #([0-9]+) begins shift`)
	sleeps := regexp.MustCompile(`falls asleep`)
	awakens := regexp.MustCompile(`wakes up`)

	var curGuard []Sleep
	var cid int
	for _, j := range keys {

		if matches := shift.FindStringSubmatch(actions[j]); len(matches) > 1 {
			id, err := strconv.Atoi(matches[1])
			if err != nil {
				log.Fatal(err)
				return
			}
			cid = id
			curGuard = Guards[cid]
			continue
		}
		if sleeps.MatchString(actions[j]) {
			curSleep := Sleep{start: j}
			Guards[cid] = curGuard
			curGuard = append(curGuard, curSleep)
			Guards[cid] = curGuard
			continue
		}
		if awakens.MatchString(actions[j]) {
			curSleep := curGuard[len(curGuard)-1]
			curSleep.stop = j
			curGuard[len(curGuard)-1] = curSleep
			continue
		}
	}
}

func longestAsleep() (longestSleeper int) {
	var longestTime time.Duration
	for g := range Guards {
		// How long does this guard g spend asleep?
		var length time.Duration
		for _, s := range Guards[g] {
			length += s.stop.Sub(s.start)
		}
		if length > longestTime {
			longestTime = length
			longestSleeper = g
		}
	}
	return
}

func mostMinute(id int) (int, int) {
	var counter [60]int
	for _, s := range Guards[id] {
		for m := s.start.Minute(); m < s.stop.Minute(); m++ {
			counter[m]++
		}
	}
	mostMinutes := 0
	minute := 0
	for m := range counter {
		if counter[m] > mostMinutes {
			mostMinutes = counter[m]
			minute = m
		}
	}
	return minute, mostMinutes
}

// Day4Part1 returns the guard who slept the most * the most slept minute
func Day4Part1(r io.Reader) int {
	parseGuardSleeps(r)
	sleepy := longestAsleep()
	minute, _ := mostMinute(sleepy)
	return sleepy * minute
}

// Day4Part2 doesn't need a file as it's already read by part1
func Day4Part2() int {
	bigSleeper := 0
	bigMinute := 0
	bigFrequency := 0
	for gid := range Guards {
		minute, times := mostMinute(gid)
		if times > bigFrequency {
			bigSleeper = gid
			bigMinute = minute
			bigFrequency = times
		}
	}

	return bigSleeper * bigMinute
}
