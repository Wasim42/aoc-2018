package day17

import (
	"strings"
	"testing"
)

func TestPart1(t *testing.T) {
	SetDebug(true)
	input := strings.NewReader(`
x=495, y=2..7
y=7, x=495..501
x=501, y=3..7
x=498, y=2..4
x=506, y=1..2
x=498, y=10..13
x=504, y=10..13
y=13, x=498..504`)
	expected := 57
	answer := Part1(input)

	if answer != expected {
		t.Errorf("Expected %d, got %d instead", expected, answer)
	}

}
