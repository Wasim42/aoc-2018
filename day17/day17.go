package day17

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"time"
)

// The very left of the pits
var lhs int
var thePits [][]rune

type location struct {
	x int
	y int
}

var top int

func parseClay(r io.Reader) {
	// We read the locations of clay, and make a list of each location.
	var clay []location

	// The one location we know is at 500,0 - which is where the water source
	// lives
	farleft := 500
	bottom := 0
	top = math.MaxInt32
	farright := 500

	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		if len(scanner.Text()) < 3 {
			// Not valid
			continue
		}
		// The format is:
		// (x|y)= int, (y|x)=lowerint..upperint
		// single number  many lower upper
		var single, many rune
		var number, lower, upper int
		var x, y *int

		fmt.Sscanf(scanner.Text(), "%c=%d, %c=%d..%d",
			&single, &number, &many, &lower, &upper)

		// Now generate each individual entry
		i := lower
		switch single {
		case 'x':
			x = &number
			y = &i
		case 'y':
			y = &number
			x = &i
		default:
			fmt.Printf("Single claims to be %c\n", single)
			panic("Invalid x/y value")
		}

		for i = lower; i <= upper; i++ {
			clay = append(clay, location{*x, *y})
			if *x < farleft {
				farleft = *x
			}
			if *x > farright {
				farright = *x
			}
			if *y > bottom {
				bottom = *y
			}
			if *y < top {
				top = *y
			}

		}
	}

	if debug {
		fmt.Printf("Area: %d -> %d, and %d deep\n", farleft, farright, bottom)
		fmt.Printf("There are %d blocks of clay\n", len(clay))
	}

	// We need two spaces to the sides
	lhs = farleft - 2
	farright++
	length := 4 + farright - farleft
	for y := 0; y <= bottom; y++ {
		thePits = append(thePits, make([]rune, length))
	}

	// Now fill in the clay
	for _, c := range clay {
		thePits[c.y][c.x-lhs] = '#'
	}

}

func printThePits() {
	// We'll never print the real world as it's *huge*
	// Assume maximum two-digits for y values, and 3 digits for x values

	// Print the x values... the hundreds
	fmt.Print("   ")
	for x := range thePits[0] {
		fmt.Print((x + lhs) / 100)
	}
	fmt.Println()
	// Print the x values... the tens
	fmt.Print("   ")
	for x := range thePits[0] {
		fmt.Print(((x + lhs) % 100) / 10)
	}
	fmt.Println()
	// Print the x values... the units
	fmt.Print("   ")
	for x := range thePits[0] {
		fmt.Print((x + lhs) % 10)
	}
	fmt.Println()

	for y := range thePits {
		fmt.Printf("%2d ", y)
		row := thePits[y]
		for x, c := range row {
			if y == 0 && x+lhs == 500 {
				fmt.Print("+")
				continue
			}
			switch c {
			case 0:
				fmt.Print(".")
			default:
				fmt.Printf("%c", c)
			}
		}
		fmt.Println()
	}
}

var debug bool

// Location holds x,y
type Location [2]int

// Queue is a fifo structure
type Queue []interface{}

func (q *Queue) push(thing interface{}) {
	if q == nil {
		*q = make([]interface{}, 0)
	}
	*q = append(*q, thing)
}

func (q *Queue) pop() (thing interface{}) {
	if len(*q) < 1 {
		return
	}
	thing = (*q)[0]
	*q = (*q)[1:]
	return
}

func (q *Queue) empty() bool {
	return len(*q) == 0
}

func (q *Queue) print() {
	for _, p := range *q {
		// They are all Locations!
		i := p.(Location)
		fmt.Printf("[%d, %d]", i[0]+lhs, i[1])
	}
	fmt.Println()
}

// pourWater is a recursive function that imitates water
func pourWater(x, y int) {
	// Our stop point is the bottom of the pits
	if y > len(thePits) {
		return
	}
	if debug {
		printThePits()
		time.Sleep(50 * time.Millisecond)
	}
	//fmt.Println("Currently:", countWater())
	// Make our mark
	thePits[y][x] = '|'
	if y+1 == len(thePits) {
		return
	}
	// Can we go down?
	below := thePits[y+1][x]
	if below == 0 || below == '|' {
		pourWater(x, y+1)
		// Now let's see if it's still just wet sand
		if thePits[y+1][x] == '|' {
			return
		}
	}
	// We have got to some clay bedrock - we need to go to both ends, and
	// determine if it falls of the edge, or if it fills up.

	// Examine the left
	fallingLeft := false
	farleft := x
	for ; ; farleft-- {
		left := thePits[y][farleft-1]
		belowLeft := thePits[y+1][farleft-1]
		if left == '#' {
			break
		}
		if belowLeft != '#' && belowLeft != '~' {
			fallingLeft = true
			farleft--
			break
		}
	}
	fallingRight := false
	farright := x
	for ; ; farright++ {
		right := thePits[y][farright+1]
		belowRight := thePits[y+1][farright+1]
		if right == '#' {
			break
		}
		if belowRight != '#' && belowRight != '~' {
			fallingRight = true
			farright++
			break
		}
	}
	// Now to fill in all the visited flat area
	filling := '~'
	if fallingLeft || fallingRight {
		filling = '|'
	}
	for i := farleft; i <= farright; i++ {
		thePits[y][i] = filling
	}
	if fallingLeft {
		pourWater(farleft, y)
	}
	if fallingRight {
		pourWater(farright, y)
	}
}

// SetDebug decides whether we print extra debugging
func SetDebug(choice bool) {
	debug = choice
}

func countWater(all bool) int {
	water := 0
	for i, row := range thePits {
		if i < top {
			continue
		}
		for _, x := range row {
			if x == '~' {
				water++
				continue
			}
			if all && x == '|' {
				water++
			}
		}
	}
	return water
}

// Part1 returns the number of watered squares
func Part1(r io.Reader) int {
	parseClay(r)
	if debug {
		printThePits()
	}
	// We start at the spring
	pourWater(500-lhs, 1)
	if debug {
		printThePits()
	}
	return countWater(true)
}

// Part2 only returns the number of standing water squares
func Part2(r io.Reader) int {
	Part1(r)
	return countWater(false)
}
