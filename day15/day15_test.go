package day15

import (
	"io"
	"strings"
	"testing"
)

type battleAnswers struct {
	input   io.Reader
	outcome int
}

func TestDay15Part1(t *testing.T) {
	kats := []battleAnswers{
		{input: strings.NewReader(`
#######
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#
#######`), outcome: 27730},
		{input: strings.NewReader(`
#######
#G..#E#
#E#E.E#
#G.##.#
#...#E#
#...E.#
#######`), outcome: 36334},
		// TODO: Add more test cases
	}
	for k, kat := range kats {
		answer := Part1(kat.input)
		if answer != kat.outcome {
			t.Errorf("%d FAILED: %d != %d", k, answer, kat.outcome)
		}
	}
}

func TestDay15Part2(t *testing.T) {
	kats := []battleAnswers{
		{input: strings.NewReader(`
#######
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#
#######`), outcome: 4988},
		// TODO: Add more test cases
	}
	for k, kat := range kats {
		answer := Part2(kat.input)
		if answer != kat.outcome {
			t.Errorf("%d FAILED: %d != %d", k, answer, kat.outcome)
		}
	}
}
