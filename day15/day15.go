package day15

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"strings"
)

// location holds an x and y location
type location struct {
	x int
	y int
}

type entrant struct {
	camp   rune // 'G' or 'E'
	attack int  // Force of attack
	hits   int  // How many hits till death
	round  int  // Which was our last move?
	loc    location
}

// bfsNode is used for the Lee algorithm - it stores information about a location
type bfsNode struct {
	x    int
	y    int
	dist int // minimum distance from the source
}

// Queue is a fifo structure
type Queue struct {
	items []interface{}
}

func (q *Queue) push(thing interface{}) {
	if q.items == nil {
		q.items = make([]interface{}, 0)
	}
	q.items = append(q.items, thing)
}

func (q *Queue) pop() (thing interface{}) {
	if len(q.items) < 1 {
		return
	}
	thing = q.items[0]
	q.items = q.items[1:]
	return
}

func (q *Queue) empty() bool {
	return len(q.items) == 0
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func manDistance(a, b location) int {
	return abs(a.x-b.x) + abs(a.y-b.y)
}

// returns a matrix of distance data for each from
func getNearestMaps(dest location, froms []location) map[location][][]int {
	matrices := map[location][][]int{}

	for _, f := range froms {
		matrices[f] = bfs(f, dest)
	}

	return matrices
}

// Given a location, it will work out all the distances of all positions from
// that location, and return that matrix
func bfs(from, to location) [][]int {

	// Initialise our distance tracker, to be of the same size as the world
	distances := make([][]int, len(world))
	for i := range distances {
		distances[i] = make([]int, len(world[i]))
	}
	// Set all the values to invalid distances
	for _, row := range distances {
		for x := range row {
			row[x] = math.MaxInt32
		}
	}

	// Create an empty queue
	q := Queue{}

	// Mark source cell as visited and enqueue the source node
	distances[from.y][from.x] = 0
	q.push(bfsNode{from.x, from.y, 0})

	// A position is valid if we've not already seen it, and it's empty
	// space.
	isValid := func(newPos, to location) bool {
		// Check the limits first
		if newPos.y < 0 {
			return false
		}
		if newPos.x < 0 {
			return false
		}
		if newPos.y >= len(distances) {
			return false
		}
		if newPos.x >= len(distances[newPos.y]) {
			return false
		}
		if distances[newPos.y][newPos.x] != math.MaxInt32 {
			return false
		}
		// If newPos == to, it's definitely valid!
		if newPos == to {
			return true
		}
		c := world[newPos.y][newPos.x]
		if c == nil {
			return true
		}
		return false
	}

	processPos := func(newPos location, dist int) {
		if isValid(newPos, to) {
			distances[newPos.y][newPos.x] = dist + 1
			q.push(bfsNode{newPos.x, newPos.y, dist + 1})
		}
	}

	for !q.empty() {
		node := q.pop().(bfsNode)

		// (i,j) represents the current location and dist stores its minimum
		// distance from the source
		i := node.x
		j := node.y
		dist := node.dist
		if i == to.x && j == to.y {
			// Found it!
			distances[j][i] = dist
			return distances
		}
		// Check for all 4 possible movements from the current location, and
		// enqueue each valid movement
		// Up, left, right, down
		processPos(location{i, j - 1}, dist)
		processPos(location{i - 1, j}, dist)
		processPos(location{i + 1, j}, dist)
		processPos(location{i, j + 1}, dist)
	}
	return distances
}

// newCombatant returns a new entrant, with combatant values
func newCombatant(camp rune, x, y, attack int) *entrant {
	nc := entrant{camp: camp, attack: attack, hits: 200, loc: location{x, y}}
	return &nc
}

func (c *entrant) String() string {
	return fmt.Sprintf("%c(%d)", c.camp, c.hits)
}

// newWall returns a wall entrant
func newWall(x, y int) *entrant {
	nw := entrant{camp: '#', loc: location{x, y}}
	return &nw
}

func (c *entrant) myEnemy() (enemy rune) {
	enemy = 'G'
	if c.camp == 'G' {
		enemy = 'E'
	}
	return
}

func (c *entrant) doAttack(hit int) {
	c.hits -= hit
	// Is this a candidate for deleting?
	if c.hits <= 0 {
		world[c.loc.y][c.loc.x] = nil
	}
}

func (c *entrant) inRange() []location {
	// Check the squares around this combatant, and see if they are clear
	inRange := make([]location, 0)
	diffx := []int{-1, 1, 0, 0}
	diffy := []int{0, 0, -1, 1}
	for i := range diffx {
		loc := location{c.loc.x, c.loc.y}
		loc.x = loc.x + diffx[i]
		loc.y = loc.y + diffy[i]
		if worldLoc(loc) == nil {
			inRange = append(inRange, loc)
		}
	}
	return inRange
}

func allReachableFrom(start location) []location {
	canReach := map[location]bool{}
	// A recursive algorithm that fills canReach
	reachable(start, &canReach)
	places := []location{}
	for r := range canReach {
		places = append(places, r)
	}
	return places
}

func reachable(start location, canReach *map[location]bool) {
	examine := func(l location) {
		if _, ok := (*canReach)[l]; !ok {
			if worldLoc(l) == nil {
				(*canReach)[l] = true
				reachable(l, canReach)
			}
		}
	}
	// Look right
	examine(location{start.x + 1, start.y})
	// Look left
	examine(location{start.x - 1, start.y})
	// Look up
	examine(location{start.x, start.y - 1})
	// Look down
	examine(location{start.x, start.y + 1})
}

func filterReachable(start location, places []location) []location {
	// From a given start position, we get a full list of all locations that we
	// can get to, and where they correspond to locations in our places, those
	// are the ones we return
	allReachablePlaces := allReachableFrom(start)
	inPlaces := func(p location) bool {
		for _, l := range places {
			if p == l {
				return true
			}
		}
		return false
	}
	reachablePlaces := []location{}
	for _, p := range allReachablePlaces {
		if inPlaces(p) {
			reachablePlaces = append(reachablePlaces, p)
		}
	}
	return reachablePlaces
}

func allCombatants(camp rune) []*entrant {
	enemies := make([]*entrant, 0)
	for _, r := range world {
		for _, c := range r {
			if c == nil {
				continue
			}
			if c.camp == camp {
				enemies = append(enemies, c)
			}
		}
	}
	return enemies
}

// Returns the location that is the first in Reading Order; ie the one with (a)
// the lowest y value, and then (b) with the lowest x value
func firstReadingOrder(places []location) location {
	x := -1
	y := places[0].y
	// Find the lowest y
	for _, p := range places {
		if y >= p.y {
			y = p.y
			x = p.x // set it to a konwn possibly valid number
		}
	}

	// Find the lowest x of the lowest y
	for _, p := range places {
		if p.y == y {
			if p.x < x {
				x = p.x
			}
		}
	}
	return location{x, y}
}

// returns the location where we should move to
func firstShortest(from, to location) location {
	// Find a list of valid locations
	validLocation := []location{}
	appendEmpty := func(inv location) {
		if worldLoc(inv) == nil {
			validLocation = append(validLocation, inv)
		}
	}
	appendEmpty(location{from.x, from.y + 1})
	appendEmpty(location{from.x, from.y - 1})
	appendEmpty(location{from.x + 1, from.y})
	appendEmpty(location{from.x - 1, from.y})

	// If to is in the list of valid locations, just jump to it!
	for _, p := range validLocation {
		if p == to {
			return p
		}
	}
	return location{-1, -1}
}

// returns false if the game is over
func (c *entrant) move(round int) (unfinished bool) {
	if c.camp == '#' {
		return true
	}
	if c.round >= round {
		// Already made the move
		return true
	}
	c.round = round
	eType := c.myEnemy()
	enemies := allCombatants(eType)
	if len(enemies) == 0 {
		// GAME OVER
		return false
	}
	unfinished = true
	// If we're next to a combatant, don't move!
	makeMove := true
	for _, e := range enemies {
		if manDistance(c.loc, e.loc) == 1 {
			makeMove = false
		}
	}

	if makeMove {

		inRange := make([]location, 0)
		for _, e := range enemies {
			inRange = append(inRange, e.inRange()...)
		}
		reachable := filterReachable(c.loc, inRange)

		if len(reachable) > 0 {

			//fmt.Println("Reachable from", c)
			//printWorld(round-1, markOut(reachable, '@'))

			// We not only get the nearest ones (ie the ones with the shortest
			// path) but the first move on that path.
			nearestMaps := getNearestMaps(c.loc, reachable)
			//for _, l := range reachable {
			//	fmt.Println("Distances from", l)
			//	printWorld(round-1, distMap(nearestMaps[l]))
			//}
			// We now need to find the nearest reachable.  Our nearestMaps has
			// all the information we need
			minDistance := math.MaxInt32
			for _, m := range nearestMaps {
				// for y := range m {
				// 	for x := range m[y] {
				// 		fmt.Printf("%d ", m[y][x])
				// 	}
				// 	fmt.Println()
				// }
				dist := m[c.loc.y][c.loc.x]
				// fmt.Println("Distance at", c.loc, dist)
				if dist < minDistance {
					minDistance = dist
				}
			}
			// Now get the reachables that are minDistance away
			nearests := []location{}
			for l, m := range nearestMaps {
				if m[c.loc.y][c.loc.x] == minDistance {
					nearests = append(nearests, l)
				}
			}
			//fmt.Println("Nearest from", c)
			//printWorld(round-1, markOut(nearests, '!'))

			chosen := firstReadingOrder(nearests)
			//fmt.Println("Chosen from", c)
			//printWorld(round-1, markOut([]location{chosen}, '+'))

			chosenMap := nearestMaps[chosen]
			//fmt.Println("Chosen map:")
			//printWorld(round-1, distMap(chosenMap))
			// We need to move to the position that is first reading order from c.loc
			// on chosenMap, such that the distance is minDistance - 1
			desiredDistance := minDistance - 1
			//fmt.Println("Looking for distance", desiredDistance)
			// Up, left, right, down
			moves := []location{
				{c.loc.x, c.loc.y - 1},
				{c.loc.x - 1, c.loc.y},
				{c.loc.x + 1, c.loc.y},
				{c.loc.x, c.loc.y + 1}}
			var step location
			for _, move := range moves {
				//fmt.Println("Found", move, chosenMap[move.y][move.x])
				if chosenMap[move.y][move.x] == desiredDistance {
					step = move
					break
				}
			}
			// //fmt.Println(c, c.loc, "heading to", chosen)
			// step := nearestMoves[chosen]
			// fmt.Println("Going to ", step)
			world[c.loc.y][c.loc.x] = nil
			c.loc = step
			world[c.loc.y][c.loc.x] = c
		}
	}
	// We attack the weakest combatant - so find them
	combatant := []*entrant{}
	addCombatant := func(enemy *entrant) {
		if enemy != nil && enemy.camp == eType {
			combatant = append(combatant, enemy)
		}
	}
	addCombatant(worldLoc(location{c.loc.x, c.loc.y - 1}))
	addCombatant(worldLoc(location{c.loc.x - 1, c.loc.y}))
	addCombatant(worldLoc(location{c.loc.x + 1, c.loc.y}))
	addCombatant(worldLoc(location{c.loc.x, c.loc.y + 1}))
	if len(combatant) < 1 {
		// No one to attack
		return
	}

	weakest := combatant[0].hits
	for _, c := range combatant {
		if weakest > c.hits {
			weakest = c.hits
		}
	}
	// Now find the combatants that are the weakest
	weaklings := []*entrant{}
	for _, c := range combatant {
		if c.hits == weakest {
			weaklings = append(weaklings, c)
		}
	}
	// If there are more than one, the first one would be the first in reading
	// order as that's the order we added them to the combatant list
	weaklings[0].doAttack(c.attack)
	return
}

func worldLoc(l location) *entrant {
	return world[l.y][l.x]
}

func doRound(round int) bool {
	for _, r := range world {
		for _, c := range r {
			if c != nil {
				if c.move(round) == false {
					return false
				}
			}
		}
	}
	return true
}

var world [][]*entrant

func setupWorld(r io.Reader, elfPower, goblinPower int) {
	world = make([][]*entrant, 0)
	scanner := bufio.NewScanner(r)
	for y := 0; scanner.Scan(); y++ {
		if len(scanner.Text()) == 0 {
			// Skip blank lines, and don't count them
			y--
			continue
		}
		newRow := make([]*entrant, len(scanner.Text()))

		for x, e := range scanner.Text() {
			if e == '#' {
				newRow[x] = newWall(x, y)
				continue
			}
			if e == 'E' {
				newRow[x] = newCombatant(e, x, y, elfPower)
				continue
			}
			if e == 'G' {
				newRow[x] = newCombatant(e, x, y, goblinPower)
				continue
			}
			// Ignore whitespace - marked by a '.'
		}
		world = append(world, newRow)
	}
}

type spaceInvader func(location) rune

func markOut(ls []location, m rune) spaceInvader {
	return func(l location) rune {
		for _, p := range ls {
			if l == p {
				return m
			}
		}
		return '.'
	}
}

func distMap(distances [][]int) spaceInvader {
	return func(l location) rune {
		d := distances[l.y][l.x]
		if d != math.MaxInt32 {
			return '0' + rune(d)
		}
		return '.'
	}
}

func dots(l location) rune {
	return '.'
}

func printWorld(round int, f spaceInvader) {
	if round == 0 {
		fmt.Println("Initially:")
	} else {
		fmt.Println("After round", round)
	}

	for y := range world {
		combs := make([]*entrant, 0)
		for x, e := range world[y] {
			if e == nil {
				fmt.Printf("%c", f(location{x, y}))
				continue
			}
			fmt.Printf("%c", e.camp)
			if e.attack > 0 {
				// An actual combatant
				combs = append(combs, e)
			}
		}
		fmt.Printf("   ")
		for i, c := range combs {
			if i > 0 {
				fmt.Printf(", ")
			}
			fmt.Printf("%s", c)
		}
		fmt.Println()
	}
}

func totUpHits() int {
	tot := 0
	for y := range world {
		for _, c := range world[y] {
			if c != nil {
				tot += c.hits
			}
		}
	}
	return tot
}

// Part1 returns the outcome given a battle
func Part1(r io.Reader) int {
	setupWorld(r, 3, 3)
	// printWorld(0, dots)
	round := 0
	for round = 1; doRound(round); round++ {
		// printWorld(round, dots)
		// time.Sleep(10 * time.Millisecond)
	}
	return (round - 1) * totUpHits()
}

// Part2 returns the outcome of minimum elf power to win
func Part2(r io.Reader) int {
	var worldData string
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		worldData += scanner.Text() + "\n"
	}
	elfPower := 3
	goblinPower := 3
	for {
		r = strings.NewReader(worldData)
		//fmt.Println("Trying elfpower", elfPower)
		setupWorld(r, elfPower, goblinPower)
		//fmt.Println("Set up world")
		elfPower++
		totElves := len(allCombatants('E'))
		round := 0
		for round = 1; doRound(round); round++ {
			//printWorld(round, dots)
			//time.Sleep(10 * time.Millisecond)
		}
		//fmt.Println("Started with", totElves)
		//fmt.Println("Finished with", len(allCombatants('E')))
		if len(allCombatants('E')) == totElves {
			return (round - 1) * totUpHits()
		}
	}
}
