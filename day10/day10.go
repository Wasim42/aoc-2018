package day10

import (
	"bufio"
	"fmt"
	"io"
	"time"
)

// Star only holds the velocity - its location is held by the hashmap of stars
type Star struct {
	vx int
	vy int
}

// Location holds the current location of a star
type Location struct {
	x int
	y int
}

// It's always possible that two stars *might* be at the same location at one
// time
func parseInputs(r io.Reader) map[Location][]Star {
	stars := make(map[Location][]Star, 0)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		var x, y, vx, vy int
		fmt.Sscanf(scanner.Text(), "position=<%d,%d> velocity=<%d,%d>",
			&x, &y, &vx, &vy)
		star := Star{vx, vy}
		location := Location{x, y}
		s, ok := stars[location]
		if ok {
			s = append(s, star)
		} else {
			s = make([]Star, 1)
			s[0] = star
		}
		stars[location] = s
	}
	return stars
}

func printStarField(stars map[Location][]Star, seconds int) {
	// Step 1 - find the boundaries
	var minx, miny, maxx, maxy int
	for l := range stars {
		if l.x < minx {
			minx = l.x
		}
		if l.x > maxx {
			maxx = l.x
		}
		if l.y < miny {
			miny = l.y
		}
		if l.y > maxy {
			maxy = l.y
		}
	}
	// If it's too big, we can't display it, so don't bother
	if maxx-minx > 300 || maxy-miny > 300 {
		return
	}
	// Step 2 - print the starfield
	for y := miny; y <= maxy; y++ {
		for x := minx; x <= maxx; x++ {
			_, ok := stars[Location{x, y}]
			if ok {
				fmt.Printf("#")
			} else {
				fmt.Printf(" ")
			}
		}
		fmt.Println()
	}
	fmt.Println("========== After ", seconds, " seconds ========")
	time.Sleep(1 * time.Second)
}

func applyMotion(stars map[Location][]Star) map[Location][]Star {
	// fmt.Println(stars)
	newStars := make(map[Location][]Star, 0)
	for loc, s := range stars {
		for _, star := range s {
			newLoc := Location{loc.x + star.vx, loc.y + star.vy}
			newS, ok := newStars[newLoc]
			if ok {
				newS = append(newS, star)
			} else {
				newS = make([]Star, 1)
				newS[0] = star
			}
			newStars[newLoc] = newS
		}
	}
	// fmt.Println(stars)
	return newStars
}

// Day10Part1 runs through the input values
func Day10Part1(r io.Reader) bool {
	stars := parseInputs(r)
	for i := 0; i < 20000; i++ {
		printStarField(stars, i)
		stars = applyMotion(stars)
	}
	return true
}
