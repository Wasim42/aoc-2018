package day2

import (
	"bufio"
	"io"
)

func Day2Part1(r io.Reader) int {
	scanner := bufio.NewScanner(r)
	twochars := 0
	threechars := 0

	for scanner.Scan() {
		code := scanner.Text()
		count := make(map[rune]int)
		for _, c := range code {
			count[c]++
		}

		var foundtwo, foundthree bool
		for _, c := range count {
			switch c {
			case 2:
				foundtwo = true
			case 3:
				foundthree = true
			}
		}
		if foundtwo {
			twochars++
		}
		if foundthree {
			threechars++
		}
	}

	return twochars * threechars

}

func Day2Part2(r io.Reader) string {
	scanner := bufio.NewScanner(r)
	bigcount := make(map[string]int)
	l := 0
	for scanner.Scan() {
		l++
		code := scanner.Text()
		// Take out one character at a time from code, and look for it in the
		// map.  If it's there, BINGO!  Otherwise just add it.
		for i := 0; i < len(code); i++ {
			value := code[0:i] + code[i+1:]
			loc, ok := bigcount[value]
			if ok && loc != l {
				// BINGO!
				return value
			}
			bigcount[value] = l
		}
	}
	return "FAILED"
}
