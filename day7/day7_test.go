package day7

import (
	"strings"
	"testing"
)

func TestDay7Part2(t *testing.T) {

	testInput := strings.NewReader(`Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.`)

	answer := Day7Part2(testInput, 2, 0)

	if answer != 15 {
		t.Errorf("Test failed, expected %d, got %d\n", 15, answer)
	}
}
