package day7

import (
	"bufio"
	"fmt"
	"io"
)

// The logic is that we store the dependencies of each item.  So, for example,
// given:
// Step C must be finished before step A can begin.
// We will store two map entries, "C" and "A".  The entry "A" will add "C" to
// its list of dependencies
func readSteps(r io.Reader) map[rune][]rune {
	bigList := make(map[rune][]rune, 0)

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		var dep, step rune
		fmt.Sscanf(scanner.Text(),
			"Step %c must be finished before step %c can begin.", &dep, &step)
		stepDeps := bigList[step]
		depDeps := bigList[dep]
		// Let's hope there aren't repeats
		stepDeps = append(stepDeps, dep)
		bigList[step] = stepDeps
		bigList[dep] = depDeps
	}

	//printBigList(bigList)

	return bigList
}

func printBigList(bigList map[rune][]rune) {
	for i := 'A'; i <= 'Z'; i++ {
		fmt.Printf("%c: ", i)
		for _, j := range bigList[i] {
			fmt.Printf("%c ", j)
		}
		fmt.Println()
	}
}

func in(order string, c rune) bool {
	for _, i := range order {
		if i == c {
			return true
		}
	}
	return false
}

func makeOrder(bigList map[rune][]rune) (order string) {
	// Keep going through bigList in alphabetical order, taking out the first
	// one that's got all its dependencies already in "order"
	for {
		for i := 'A'; i <= 'Z'; i++ {
			entry, ok := bigList[i]
			if !ok {
				// This character has already been dealt with (if it existed)
				continue
			}
			// Look through all the values in entry, and ensure that they
			// exist in order
			found := true
			for _, e := range entry {
				if !in(order, e) {
					found = false
					break
				}
			}
			if !found {
				continue
			}
			order += string(i)
			delete(bigList, i)
			break
		}
		if len(bigList) == 0 {
			return
		}
	}
}

// Day7Part1 returns the correct order of a list of dependencies
func Day7Part1(r io.Reader) string {
	bigList := readSteps(r)

	return makeOrder(bigList)
}

// Workers contains the task being worked on, and how long left till it's
// finished
type Workers struct {
	task      rune
	countdown int
}

// getATask can return 0 if there is no task ready
func getATask(bigList map[rune][]rune, completed string) rune {
	// We still look through the list in alphabetical order
	for t := 'A'; t <= 'Z'; t++ {
		if _, ok := bigList[t]; !ok {
			// Already dealt with
			continue
		}
		// Look through all the deps.  If any of the deps aren't completed,
		// then we can't do the task t
		found := true
		for _, d := range bigList[t] {
			if !in(completed, d) {
				found = false
				break
			}
		}
		if !found {
			continue
		}
		return t
	}
	return 0
}

// This moves time on by a second.
// Potentially a task could complete, and get added to the completed string
func tick(workers []Workers, completed string) ([]Workers, string) {
	for i, w := range workers {
		if w.task == 0 {
			continue
		}
		w.countdown--
		if w.countdown != 0 {
			workers[i] = w
			continue
		}
		completed += string(w.task)
		w.task = 0
		workers[i] = w
	}
	return workers, completed
}

func printWorkers(workers []Workers) {
	for _, w := range workers {
		if w.task > 0 {
			fmt.Printf(" [%c (%02d)] ", w.task, w.countdown)
		} else {
			fmt.Printf(" [0 (%02d)] ", w.countdown)
		}
	}
	fmt.Println()
}

// Day7Part2 returns the total time to complete the building, given the number
// of workers
func Day7Part2(r io.Reader, numWorkers, offset int) int {
	bigList := readSteps(r)

	// printBigList(bigList)

	var completed string
	workers := make([]Workers, numWorkers)

	totalTime := 0
	for {
		for i, w := range workers {
			if w.task == 0 {
				task := getATask(bigList, completed)
				if task == 0 {
					// No tasks available, we'll tick soon enough
					continue
				}
				// Time to assign the task, and set its countdown
				delete(bigList, task)
				w.task = task
				w.countdown = offset + 1 + int(task-'A')
				workers[i] = w
			}
		}
		// fmt.Print(totalTime, " ")
		// printWorkers(workers)
		workers, completed = tick(workers, completed)
		totalTime++
		if len(bigList) > 0 {
			continue
		}
		finished := true
		for _, w := range workers {
			if w.task != 0 {
				finished = false
			}
		}
		if finished {
			break
		}
	}

	return totalTime
}
