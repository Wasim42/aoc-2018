Note that in day 13 I introduced colourisation of the output.  To add support:

```bash
go get github.com/fatih/color
```

To run everything (which is the way it's been set up), just run:

```bash
go run main.go
```

To run a single test (eg day 12):

```bash
(cd day 12 && go test)
````
