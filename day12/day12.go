package day12

import (
	"bufio"
	"fmt"
	"io"
	"sync"
)

// PlantPot knows whether it has a plant or not
type PlantPot struct {
	num     int
	plant   bool
	pending bool
}

type pottingStruct struct {
	sync.RWMutex
	positive       []*PlantPot
	negative       []*PlantPot
	plantPotsFirst int
	plantPotsLast  int
}

var pottingShed pottingStruct

// InitPlants initialises the plants
func InitPlants(newLen int) *PlantPot {
	pottingShed.Lock()
	defer pottingShed.Unlock()
	pottingShed.positive = make([]*PlantPot, 1, newLen)
	pottingShed.negative = make([]*PlantPot, 1)
	zeroPot := PlantPot{num: 0}
	pottingShed.positive[0] = &zeroPot
	pottingShed.negative[0] = &zeroPot
	pottingShed.plantPotsFirst = 0
	pottingShed.plantPotsLast = 0
	return getPotNum(0)
}

// The write lock *must* be obtained before calling this!
func setPotNum(number int, p *PlantPot) {
	if number < pottingShed.plantPotsFirst || number > pottingShed.plantPotsLast {
		errstr := "Invalid number: " + string(number) + " Out of range " +
			string(pottingShed.plantPotsFirst) + string(pottingShed.plantPotsLast)
		panic(errstr)
	}
	if number >= 0 {
		pottingShed.positive[number] = p
	}
	if number <= 0 {
		pottingShed.negative[-number] = p
	}
}

// The appropriate lock *must* be obtained before calling this!
func getPotNum(number int) *PlantPot {
	if number < pottingShed.plantPotsFirst || number > pottingShed.plantPotsLast {
		return nil
	}
	if number > 0 {
		return pottingShed.positive[number]
	}
	return pottingShed.negative[-number]
}

// Prev returns the previous PlantPot
// if makeIt is true, it will make a new one if one doesn't already exist
func (p *PlantPot) Prev(makeIt bool) *PlantPot {
	pottingShed.RLock()
	defer pottingShed.RUnlock()
	prevNum := p.num - 1
	pot := getPotNum(prevNum)
	if pot != nil || !makeIt {
		return pot
	}
	pottingShed.RUnlock()
	// This is to make the deferred RUnlock not complain!
	defer pottingShed.RLock()
	pottingShed.Lock()
	defer pottingShed.Unlock()
	// Check that another thread hasn't added what we're about to add
	if prevNum >= pottingShed.plantPotsFirst {
		q := getPotNum(prevNum)
		if q == nil {
			panic("This should not be nil")
		}
		return q
	}

	newPot := PlantPot{num: prevNum}
	pottingShed.plantPotsFirst = prevNum
	// We have a different approach depending on whether the new prevNum is
	// positive, negative or zero!
	if prevNum > 0 {
		pottingShed.positive[prevNum] = &newPot
		q := getPotNum(prevNum)
		if q == nil {
			panic("This should not be nil")
		}
		return q
	}

	if prevNum == 0 {
		pottingShed.positive[prevNum] = &newPot
	}
	pottingShed.negative = append(pottingShed.negative, &newPot)
	q := getPotNum(prevNum)
	if q == nil {
		fmt.Println("prevNum is", prevNum)
		if prevNum > 0 {
			fmt.Println("Positive: ", pottingShed.positive)
		} else {
			fmt.Println("negative: ", pottingShed.negative)
		}
		panic("This should not be nil")
	}
	return q
}

// Next returns the next PlantPot
// if makeIt is true, it will make a new one if one doesn't already exist
func (p *PlantPot) Next(makeIt bool) *PlantPot {
	pottingShed.RLock()
	defer pottingShed.RUnlock()
	nextNum := p.num + 1
	pot := getPotNum(nextNum)
	if pot != nil || !makeIt {
		return pot
	}
	pottingShed.RUnlock()
	// This is to make the deferred RUnlock not complain!
	defer pottingShed.RLock()
	pottingShed.Lock()
	defer pottingShed.Unlock()
	// Check that another thread hasn't added what we're about to add
	if nextNum <= pottingShed.plantPotsLast {
		q := getPotNum(nextNum)
		if q == nil {
			panic("This should not be nil")
		}
		return q
	}

	newPot := PlantPot{num: nextNum}
	pottingShed.plantPotsLast = nextNum
	// We have a different approach depending on whether the new nextNum is
	// positive, negative or zero!
	if nextNum < 0 {
		pottingShed.negative[-nextNum] = &newPot
		q := getPotNum(nextNum)
		if q == nil {
			panic("This should not be nil")
		}
		return q
	}

	if nextNum == 0 {
		pottingShed.negative[nextNum] = &newPot
	}
	pottingShed.positive = append(pottingShed.positive, &newPot)
	q := getPotNum(nextNum)
	if q == nil {
		fmt.Println(pottingShed.positive)
		panic("This should not be nil")
	}
	return q
}

// Trim will ensure that there are at most 3 zeroes on each end
func (p *PlantPot) Trim() {
	pottingShed.Lock()
	defer pottingShed.Unlock()
	first := getPotNum(pottingShed.plantPotsFirst)
	second := getPotNum(pottingShed.plantPotsFirst + 1)
	third := getPotNum(pottingShed.plantPotsFirst + 2)
	for !first.plant && !second.plant && !third.plant {
		// firstNum is ready for the garbage collector
		setPotNum(pottingShed.plantPotsFirst, nil)
		pottingShed.plantPotsFirst++
		first = getPotNum(pottingShed.plantPotsFirst)
		second = getPotNum(pottingShed.plantPotsFirst + 1)
		third = getPotNum(pottingShed.plantPotsFirst + 2)
	}
	last := getPotNum(pottingShed.plantPotsLast)
	last2 := getPotNum(pottingShed.plantPotsLast - 1)
	last3 := getPotNum(pottingShed.plantPotsLast - 2)
	for !last.plant && !last2.plant && !last3.plant {
		// lastNum is ready for the garbage collector
		setPotNum(pottingShed.plantPotsLast, nil)
		pottingShed.plantPotsLast--
		last = getPotNum(pottingShed.plantPotsLast)
		last2 = getPotNum(pottingShed.plantPotsLast - 1)
		last3 = getPotNum(pottingShed.plantPotsLast - 2)
	}
	// Now to trim off the excess nils from the arrays
	if pottingShed.plantPotsFirst <= 0 {
		pottingShed.negative = pottingShed.negative[0 : (-pottingShed.plantPotsFirst)+1]
	} else if len(pottingShed.negative) > 0 {
		// negative needs to be empty
		pottingShed.negative = pottingShed.negative[0:0]
	}
	if pottingShed.plantPotsLast >= 0 {
		pottingShed.positive = pottingShed.positive[0 : (pottingShed.plantPotsLast)+1]
	} else if len(pottingShed.positive) > 0 {
		// positive needs to be empty
		pottingShed.positive = pottingShed.positive[0:0]
	}
}

// DePlant will deplant a PlantPot
func (p *PlantPot) DePlant() {
	(*p).pending = false
}

// EnPlant will put a plant in a PlantPot
func (p *PlantPot) EnPlant() {
	(*p).pending = true
}

// Tick will send the pending to plant
func (p *PlantPot) Tick() {
	(*p).plant = (*p).pending
}

// Equals checks for equality (by comparing num)
func (p *PlantPot) Equals(q *PlantPot) bool {
	if (*p).num == (*q).num {
		return true
	}
	return false
}

// StateInt returns the current state as either a 1 or a 0
func (p *PlantPot) StateInt() int {
	if (*p).plant {
		return 1
	}
	return 0
}

// CountPlant returns num if it has a plant, and zero otherwise
func (p *PlantPot) CountPlant() int {
	if (*p).plant {
		return (*p).num
	}
	return 0
}

// FirstPlant returns the first plant
func (p *PlantPot) FirstPlant() *PlantPot {
	pottingShed.RLock()
	defer pottingShed.RUnlock()
	return getPotNum(pottingShed.plantPotsFirst)
}

// LastPlant returns the last plant
func (p *PlantPot) LastPlant() *PlantPot {
	pottingShed.RLock()
	defer pottingShed.RUnlock()
	return getPotNum(pottingShed.plantPotsLast)
}

func setupInitialState(initState string) *PlantPot {

	p := InitPlants(len(initState) * 2)
	for _, s := range initState {
		if s == '#' {
			p.EnPlant()
			p.Tick()
		}
		p = p.Next(true)
	}

	return p
}
func setupRules(scanner *bufio.Scanner) []bool {
	rules := make([]bool, 2*2*2*2*2)

	for scanner.Scan() {
		var prestate string
		var outcome rune
		fmt.Sscanf(scanner.Text(), "%s => %c",
			&prestate, &outcome)
		// Convert prestate to an int by using the power of Binary
		num := 0
		bit := 32
		for _, c := range prestate {
			bit = bit / 2
			if c == '#' {
				num = num + bit
			}
		}
		if outcome == '#' {
			rules[num] = true
		} else {
			rules[num] = false
		}
	}
	return rules
}

func parseInputFile(r io.Reader) (*PlantPot, []bool) {

	scanner := bufio.NewScanner(r)
	// The first line is the initial state, so process that first
	scanner.Scan()
	var initState string
	fmt.Sscanf(scanner.Text(), "initial state: %s", &initState)
	p := setupInitialState(initState)

	// Skip the blank line
	scanner.Scan()
	rules := setupRules(scanner)

	return p, rules
}

func applyTicks(p *PlantPot) {
	for p = p.FirstPlant(); p != nil; p = p.Next(false) {
		p.Tick()
	}
}

func applyRule(p *PlantPot, rules []bool, wg *sync.WaitGroup) {
	defer wg.Done()
	prev := p.Prev(true)
	next := p.Next(true)
	prev2 := prev.Prev(true)
	next2 := next.Next(true)
	state := prev2.StateInt()*16 + prev.StateInt()*8 + p.StateInt()*4 + next.StateInt()*2 + next2.StateInt()
	if rules[state] {
		p.EnPlant()
	} else {
		p.DePlant()
	}
}

// Returns the very first interesting plantpot in the list
func applyRules(p *PlantPot, rules []bool) *PlantPot {
	lastOne := p.LastPlant()
	firstOne := p.FirstPlant()

	var wg sync.WaitGroup
	p = firstOne
	for p != nil {
		wg.Add(1)
		go applyRule(p, rules, &wg)
		if p.Equals(lastOne) {
			break
		}
		p = p.Next(false)
	}
	wg.Wait()
	return firstOne
}

func plantState(p *PlantPot) {
	p = p.FirstPlant()
	for p != nil {
		fmt.Printf("%d", p.StateInt())
		p = p.Next(false)
	}
	fmt.Println()
}

// Simply total all the num's of the plantpots that have a plant in
func totalPlants(p *PlantPot) int {
	// Step 1: Find the beginning
	p = p.FirstPlant()
	for p.Prev(false) != nil {
		p = p.Prev(false)
	}
	// Now just loop through, totalling them
	total := 0
	for total = 0; p != nil; p = p.Next(false) {
		total += p.CountPlant()
	}
	return total
}

// Day12Part1 returns the sum of the pots that contain plants
func Day12Part1(r io.Reader, generations int) int {
	p, rules := parseInputFile(r)
	// It turns out that after a while, the pattern remains static, and the
	// totals follow a simple pattern (in the provided example, I see that it
	// increases by 800k for every 10k iterations)
	// If I was fully optimising this, I would simply look for an emerging
	// pattern asap, but given that after the furious optimisations I've taken
	// it takes a single second to process 10k iterations, let's just use that
	// value.
	var first10k, second10k int
	for i := 0; i < generations && i <= 30000; i++ {
		if i == 10000 {
			first10k = totalPlants(p)
		}
		if i == 20000 {
			second10k = totalPlants(p)
		}
		p = applyRules(p, rules)
		applyTicks(p)
		p.Trim()
	}
	if second10k == 0 {
		// We've completed!
		return totalPlants(p)
	}
	per10k := second10k - first10k
	return (generations/10000)*per10k + (first10k - per10k)
}
