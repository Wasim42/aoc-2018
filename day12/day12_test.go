package day12

import (
	"strings"
	"testing"
)

func TestDay12Part1(t *testing.T) {
	inputFile := strings.NewReader(`initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #`)
	expected := 325

	answer := Day12Part1(inputFile, 20)

	if answer != expected {
		t.Errorf("Expected %d, got %d", expected, answer)
	}
}
